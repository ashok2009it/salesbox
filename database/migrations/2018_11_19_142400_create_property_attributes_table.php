<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ad_id');
            $table->integer('surface_size')->nullable();
            $table->integer('bedrooms')->nullable();
            $table->integer('bathrooms')->nullable();
            $table->string('type_of_property')->nullable();
            $table->boolean('listed_by')->nullable();
            $table->boolean('parking')->nullable();
            $table->boolean('broker')->nullable();
            $table->string('furnished')->nullable();
            $table->boolean('pets')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_attributes');
    }
}
