<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ad_id');
            $table->string('make')->nullable();
            $table->string('type')->nullable();
            $table->integer('year')->nullable();
            $table->integer('mileage')->nullable();
            $table->integer('fuel')->nullable();
            $table->string('color')->nullable();
            $table->boolean('first_owner')->nullable();
            $table->boolean('condition')->nullable();
            $table->boolean('transmission')->nullable();
            $table->boolean('listed_by')->nullable();
            $table->string('vin_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_attributes');
    }
}
