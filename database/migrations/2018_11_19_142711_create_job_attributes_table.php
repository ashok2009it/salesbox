<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ad_id');
            $table->integer('type')->nullable()->comment('seeker or employer');
            $table->string('company_name')->nullable();
            $table->string('position')->nullable();
            $table->string('experience_min')->nullable();
            $table->string('experience_max')->nullable();
            $table->integer('salary_period')->nullable()->comment('hourly weekly etc');
            $table->string('salary_min')->nullable();
            $table->string('salary_max')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_attributes');
    }
}
