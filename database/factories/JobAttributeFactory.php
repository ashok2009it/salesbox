<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category\JobAttribute::class, function (Faker $faker) {
    return [
        'ad_id' => function () {
            return factory(App\Models\Ad::class)->create()->id;
        },
    ];
});
