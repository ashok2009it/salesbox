<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Ad::class, function (Faker $faker) {
    $title = $faker->unique()->word;
    return [
        'title' => $title,
        'slug' => str_slug($title, '-'),
        'price' => $faker->randomNumber(),
        'category_id' => $faker->numberBetween(1, 20),
        'description' => $faker->sentence,
        'status' => 1,
        'created_by' => 1,
        'featured' => 0,
        'city_id' => 1,
    ];
});
