<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Report::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory('App\Models\User')->create()->id;
        },
        'ad_id' => function () {
            return factory('App\Models\Ad')->create()->id;
        },
        'status' => 1,
        'message' => $faker->sentence
    ];
});
