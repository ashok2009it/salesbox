<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Help\Help::class, function (Faker $faker) {
    $question = $faker->unique()->sentence;
    return [
        'question' => $question,
        'slug' => str_slug($question, '-'),
        'answer' => $faker->sentence,
        'popular' => 0,
        'status' => 0,
        'category_id' => function () {
            return factory(App\Models\Help\HelpCategory::class)->create()->id;
        }
    ];
});
