<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    $title = $faker->unique()->word;
    return [
        'title' => $title,
        'slug' => str_slug($title, '-'),
        'icon' => 'fa fa-rss',
        'status' => 1,
        'attribute_type' => App\Models\Category::TYPE[$faker->numberBetween(1, 4)]
    ];
});
