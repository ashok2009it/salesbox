<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Help\HelpCategory::class, function (Faker $faker) {
    $title = $faker->unique()->word;
    return [
        'title' => $title,
        'slug' => str_slug($title, '-'),
        'parent_id' => null
    ];
});
