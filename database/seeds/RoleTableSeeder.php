<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Spatie\Permission\Models\Role::create(['name' => 'admin']);
        Spatie\Permission\Models\Role::create(['name' => 'vendor']);

        App\Models\User::first()->assignRole('admin');
    }
}
