<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::create(
            [
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => bcrypt('secret'),
                'remember_token' => str_random(10),
                'status'         => true,
                'email_verified_at' => '2018-12-24 12:12:52'
            ]
        );
        
        App\Models\User::create(
            [
                'name'           => 'test',
                'email'          => 'test@test.com',
                'password'       => bcrypt('secret'),
                'remember_token' => str_random(10),
                'status'         => true
            ]
        );
    }
}
