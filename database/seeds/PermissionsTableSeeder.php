<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
        	'dashboards',
        	'users',
        	'roles',
            'ads',
            'city',
            'category',
            'report',
            'setting',
            'help_category',
            'help',
        ];

        foreach ($permissions as $key => $permission) {
        	Artisan::call('sellsbox:auth:permission', ['name' => $permission]);
        }
    }
}
