<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 25) as $index) {
            $category = \App\Models\Category::find($faker->randomElement([2, 4, 6, 8, 10, 12, 14, 16, 18, 20]));
            factory(\App\Models\Ad::class)->create([
                'attribute_type' => $category->attribute_type,
                'category_id' => $category->id
            ]);

            factory("App\Models\Category\\".$category->attribute_type)->create([
                'ad_id' => $index
            ]);
        }
    }
}
