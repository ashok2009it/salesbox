<?php

use Illuminate\Database\Seeder;

class HelpCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Help\HelpCategory::class, 5)->create();
        
        foreach(range(1, 5) as $index) {
            factory(\App\Models\Help\HelpCategory::class)->create([
                'parent_id' => rand(1, 5)
            ]);
        }
    }
}
