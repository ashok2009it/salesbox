<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            $type = \App\Models\Category::TYPE[$faker->numberBetween(0, 3)];
            $title = $faker->unique()->word;
            $category = \App\Models\Category::create([
                'title' => $title,
                'slug' => str_slug($title, '-'),
                'icon' => 'fa fa-rss',
                'status' => 1,
                'attribute_type' => $type
            ]);

            $title = $faker->unique()->word;
            \App\Models\Category::create([
                'title' => $title,
                'slug' => str_slug($title, '-'),
                'icon' => 'fa fa-rss',
                'status' => 1,
                'parent_id' => $category->id,
                'attribute_type' => $type,
                'attributes' => json_encode($category->columns('App\Models\Category\\'.$type))
            ]);
        }
    }
}
