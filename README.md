# SellsBox

## Setting up development environment

    // make sure docker and docker-compose is installed in the host machine

    $ cp .env.example .env
    $ php artisan key:generate
    $ composer install
    $ yarn
    $ npm run dev
    $ docker-compose pull
    $ docker-compose up -d
    $ chmod 777 -R storage # to fix the permission in docker
    $ docker-compose exec app php artisan migrate --seed
    $ curl -I localhost:9000 # if status is 200 we are good

## Running artisan command through docker

    $ docker-compose exec app php artisan # you can alias `docker-compose exec app php artisan` in you .bashrc or .zshrc

## Access database

    For phpmyadmin go to http://localhost:9001
    For commandline access:
    $ docker-compose exec db mysql -u root -p # default password is `toor`
