namespace :app do
  desc 'Link the public storage'
  task :link_storage do
    on roles (:app) do
      within release_path do
        execute :php, 'artisan', 'storage:link'
      end
    end
  end

  desc 'run database migration'
  task :migrate do
    on roles (:app) do
      within release_path do
        execute :php, 'artisan', 'migrate --force'
      end
    end
  end

  desc 'set the deploy info'
  task :deploy_info do
    on roles(:app) do
      release_file = "#{release_path}/public/v.txt"
      execute "touch #{release_path}
      echo 'Date:' #{Time.now.iso8601.to_s} >> #{release_file}
      echo 'Branch:' #{fetch(:branch)} >> #{release_file}
      echo 'Revision:' #{fetch(:current_revision)} >> #{release_file}
      echo 'Deployed By:' #{local_user} >> #{release_file}"
    end
  end

  desc 'set up env for staging deployment'
  task :set_up_env_for_staging do
    on roles(:app) do
      execute :cp, "~/credentials/.env #{release_path}/.env"
    end
  end

  desc 'cache config & route'
  task :cache_config_and_route do
    on roles (:app) do
      within release_path do
        # execute :php , 'artisan config:clear'
        # execute :php , 'artisan config:cache'

        execute :php , 'artisan route:clear'
        execute :php , 'artisan route:cache'
      end
    end
  end

  desc 'artisan command'
  task :artisan , [:command_name] do |_t, args|
    on roles (:app) do
      within release_path do
        execute :php , 'artisan', args[:command_name]
      end
    end
  end

  desc 'create dir'
  task :create_dir do
    on roles (:app) do
        execute "mkdir -p #{shared_path}/storage/app/public
         mkdir -p #{shared_path}/storage/logs
         mkdir -p #{shared_path}/storage/framework/cache
         mkdir -p #{shared_path}/storage/framework/sessions
         mkdir -p #{shared_path}/storage/framework/views"
    end
  end

  desc 'run yarn'
  task :yarn do
    on roles(:app) do
      within release_path do
        execute :yarn, "install --no-progress"
      end
    end
  end

  desc 'run npm comilation'
  task :npm_compile do
    on roles(:app) do
      within release_path do
        execute :npm, "run prod"
      end
    end
  end

#   desc 'copy assets from s3'
#   task :copy_assets_from_s3 do
#     on roles(:app) do
#       within release_path do
#         file_count = capture("aws s3 ls s3://sabkura-assets/#{fetch(:current_revision)}/ | wc -l")
#         if file_count.to_i == 0
#           invoke 'app:yarn'
#           invoke 'app:npm_compile'
#         end
#         execute :aws, "s3 sync s3://sabkura-assets/#{fetch(:current_revision)}/ ./public/"
#       end
#     end
#   end
end
