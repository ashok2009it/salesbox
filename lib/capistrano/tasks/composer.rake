namespace :composer do
  desc 'Running Composer Install'
  task :install do
    on roles(:app) do
      within release_path do
        execute :composer, 'install --no-dev --no-interaction --quiet --no-progress'
      end
    end
  end
end
