# Devops commands
namespace :ops do
  desc 'reload the nginx server'
  task :reload_server do
    on roles(:app) do
      execute :sudo, :service, 'nginx reload'
    end
  end

  desc 'reload php-fpm'
  task :reload_phpfpm do
    on roles(:app) do
      execute :sudo, :service, 'php7.2-fpm reload'
    end
  end

  desc 'Restart Queue'
  task :queue_restart do
    on roles (:app) do
      within release_path do
        execute :php , 'artisan', 'queue:restart'
      end
    end
  end
end