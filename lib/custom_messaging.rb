module Slackistrano
  class CustomMessaging < Messaging::Base

    # Send failed message to #ops. Send all other messages to default channels.
    # The #ops channel must exist prior.
    def channels_for(action)
      if action == :failed
        '#ops'
      else
        super
      end
    end

    def payload_for_reverting
      {
        attachments: [{
          color: 'good',
          title: "#{deployer} has finished rollback to #{ENV['ROLLBACK_RELEASE']} :boom::bangbang:",
          fields: [{
            title: 'Environment',
            value: stage,
            short: true
          }, {
            title: 'App',
            value: application,
            short: true
          }, {
            title: 'Branch',
            value: branch,
            short: true
          }, {
            title: 'Deployer',
            value: deployer,
            short: true
          }, {
            title: 'Time',
            value: elapsed_time,
            short: true
          }],
          fallback: super[:text]
        }]
      }
    end

    # Fancy updated message.
    # See https://api.slack.com/docs/message-attachments
    def payload_for_updated
      {
        attachments: [{
          color: 'good',
          title: "#{deployer} has finished deploying :boom::bangbang:",
          fields: [{
            title: 'Environment',
            value: stage,
            short: true
          }, {
            title: 'App',
            value: application,
            short: true
          }, {
            title: 'Branch',
            value: branch,
            short: true
          }, {
            title: 'Deployer',
            value: deployer,
            short: true
          }, {
            title: 'Elapsed Time',
            value: elapsed_time,
            short: true
          }],
          fallback: super[:text]
        }]
      }
    end

    # Slightly tweaked failed message.
    # See https://api.slack.com/docs/message-formatting
    def payload_for_failed
      {
        attachments: [{
          color: 'danger',
          title: payload[:text],
          fields: [{
            title: 'Environment',
            value: stage,
            short: true
          }, {
            title: 'App',
            value: application,
            short: true
          }, {
            title: 'Branch',
            value: branch,
            short: true
          }, {
            title: 'Deployer',
            value: deployer,
            short: true
          }, {
            title: 'Elapsed Time',
            value: elapsed_time,
            short: true
          }],
          fallback: super[:text]
        }]
      }
    end

    def payload_for_updating
      {
        text: "#{deployer} has started deploying branch #{branch} of #{application} to #{stage}"
      }
    end

    def payload_for_reverted
      {
        text: "#{deployer} has finished rolling back branch of #{application} to #{stage}"
      }
    end

    # Override the deployer helper to pull the full name from the password file.
    # See https://github.com/phallstrom/slackistrano/blob/master/lib/slackistrano/messaging/helpers.rb
    def deployer
      ENV['USER']
    end
  end
end
