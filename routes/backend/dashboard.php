<?php

Route::get('/', 'DashboardController@index')->name('dashboard');
Route::resource('/users', 'UserController')->except('show');

Route::resource('/roles', 'RoleController')->except('show');
Route::post('assign-role/{id}','UserController@assignRole')->name('assign.role');

Route::resource('/ads', 'AdController');
Route::get('/ads/restore/{id}', 'AdController@restore')->name('ads.restore');
Route::get('/ads/hard-delete/{id}', 'AdController@hardDelete')->name('ads.hard-delete');

Route::resource('/cities', 'CityController')->except('show');
Route::resource('/reports', 'ReportController')->except('create', 'store');

Route::resource('/categories', 'CategoryController');
Route::get('/categories/create/{id}', 'CategoryController@createSub')->name('create.sub');
Route::get('/categories/edit/{id}', 'CategoryController@editSub')->name('edit.sub');

Route::resource('/help', 'Help\HelpController');
Route::resource('/help-categories', 'Help\HelpCategoryController')->except('show');
Route::resource('/settings', 'SettingController')->except('show');
