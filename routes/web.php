<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
//socialite routes
Route::get('/auth/{provider}', 'Auth\SocialController@redirectToProvider');
Route::get('/auth/{provider}/callback', 'Auth\SocialController@handleProviderCallback');
Route::middleware('auth')->resource('login-as', 'Auth\LoginAsController', ['only' => ['update', 'destroy']]);

/*
 * Frontend Routes
 */
Route::group([
    'namespace' => 'Frontend',
    'as' => 'frontend.',
    // 'middleware' => 'verified'
], function () {
    include_route_files(__DIR__.'/frontend/');
});

/*
 * Backend Routes
 */
Route::group([
	'namespace' => 'Backend',
	'prefix' => 'dashboard',
	'as' => 'dashboard.',
	'middleware' => ['auth', 'permission:view_dashboards'],
], function () {
    include_route_files(__DIR__.'/backend/');
});
