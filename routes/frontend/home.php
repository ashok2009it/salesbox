<?php

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home');

Route::resource('/product', 'ProductController');
Route::resource('/blog', 'BlogController');
Route::view('/user-products', 'frontend.user.dashboard.userProducts')->name('user.products');
Route::get('/search', 'ProductController@search')->name('search.product');
Route::view('/user-profile', 'frontend.user.userProfile')->name('user.profile');
Route::get('/message/{id?}', 'MessageController@index')->middleware('auth');
// Route::get('/get-messages/{sender}/{reciever}', 'MessageController@getMessage')->middleware('auth');
// Route::post('/add-message', 'MessageController@store')->middleware('auth');
