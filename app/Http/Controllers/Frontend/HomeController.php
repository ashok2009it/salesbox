<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Ad\AdInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdInterface $repository)
    {
        if (request('q')) {
            $ads = $repository->search(request('q'));
        } else {
            $ads = \App\Models\Ad::all();
        }
        return view('frontend.index', ['ads' => $ads]);
    }
}
