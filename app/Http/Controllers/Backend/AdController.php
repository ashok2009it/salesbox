<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Category\CategoryRepository;
use App\Repositories\Backend\Ad\AdRepository;
use App\Repositories\Backend\City\CityRepository;
use App\Http\Requests\Ad\AdRequest;
use Log;
use DB;

class AdController extends Controller
{
    private $ad;
    private $category;
    private $city;

    public function __construct(
        AdRepository $ad,
        CategoryRepository $category,
        CityRepository $city
    ) {
        $this->middleware('permission:view_ads', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_ads', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_ads', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_ads', ['only' => 'destroy']);

        $this->ad = $ad;
        $this->category = $category;
        $this->city = $city;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request('deleted')) {
            $ads = $this->ad->deletedAll();
        } else {
            $ads = $this->ad->all();
        }

        return view('backend.ad.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesList = $this->category->getSubCategory();
        $cities = $this->city->pluck('name', 'id');

        return view('backend.ad.create', compact('categoriesList', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->ad->create($request->validated());

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            flash(__('alerts.backend.ad.error'))->error();
            return redirect(route('dashboard.ads.index'));
        }
        flash(__('alerts.backend.ad.created'))->success();
        return redirect(route('dashboard.ads.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = $this->ad->find($id);

        return view('backend.ad.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoriesList = $this->category->getSubCategory();
        $ad = $this->ad->find($id);
        $cities = $this->city->pluck('name', 'id');

        return view('backend.ad.edit', compact('ad', 'categoriesList', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->ad->update($request->validated(), $id);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            flash(__('alerts.backend.ad.error'))->error();
            return redirect(route('dashboard.ads.index'));
        }
        flash(__('alerts.backend.ad.updated'))->success();
        return redirect(route('dashboard.ads.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->ad->delete($id);

        flash(__('alerts.backend.ad.deleted'))->success();
        return redirect(route('dashboard.ads.index'));
    }

    public function restore($id)
    {
        $this->ad->restore($id);
        flash(__('alerts.backend.ad.restored'))->success();
        return redirect(route('dashboard.ads.index', ['deleted' => true]));
    }

    public function hardDelete($id)
    {
        $this->ad->hardDelete($id);

        flash(__('alerts.backend.ad.deleted'))->success();
        return redirect(route('dashboard.ads.index', ['deleted' => true]));
    }
}
