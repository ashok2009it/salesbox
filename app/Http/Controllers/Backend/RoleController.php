<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view_roles', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_roles', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_roles', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_roles', ['only' => 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('backend.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'display_name' => 'nullable',
            'remarks' => 'nullable'
        ]);

        $role = Role::create([
            'name' => request('name'),
            'display_name' => request('display_name'),
            'remarks' => request('remarks')
        ]);

        if ($role) {
            flash('Role created')->success();
        } else {
            flash("Error on role creation")->error();
        }

        return redirect(route('dashboard.roles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = Permission::all();
        $role = Role::find($id);

        return view('backend.role.edit', compact('permissions', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        
        $this->validate($request, [
            'name' => 'required|unique:roles,name,'.$role->id,
            'display_name' => 'nullable',
            'remarks' => 'nullable'
        ]);


        $update = $role->update([
            'name' => request('name'),
            'display_name' => request('display_name'),
            'remarks' => request('remarks')
        ]);

        if ($role->name === 'admin') {
            $role->syncPermissions(Permission::all());
            
            return redirect()->route('dashboard.roles.index');
        }

        $sync = $role->syncPermissions(request('permission'));

        if ($role && $sync) {
            flash("successful update")->success();
        } else {
            flash("error on update")->error();
        }

        return redirect(route('dashboard.roles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id)->delete();

        if ($role) {
            flash("successful deleted")->success();
        } else {
            flash("error on delete")->error();
        }

        return redirect(route('dashboard.roles.index'));
    }
}
