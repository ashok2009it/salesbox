<?php

namespace App\Http\Controllers\Backend\Help;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Help\HelpCategory\HelpCategoryRepository;
use App\Http\Requests\Help\HelpCategory\HelpCategoryStoreRequest;
use App\Http\Requests\Help\HelpCategory\HelpCategoryUpdateRequest;
use Log;
use DB;

class HelpCategoryController extends Controller
{
    private $helpCategory;

    public function __construct(HelpCategoryRepository $helpCategory)
    {
        $this->helpCategory = $helpCategory;
        $this->middleware('permission:view_help_categories', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_help_categories', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_help_categories', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_help_categories', ['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->helpCategory->all();

        return view('backend.help.help-category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->helpCategory->pluck('title', 'id');

        return view('backend.help.help-category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HelpCategoryStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->helpCategory->create($request->validated());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            flash(__('alerts.backend.help.error'))->error();
            return redirect(route('dashboard.help-categories.index'));
        }

        flash(__('alerts.backend.help.help-category.created'))->success();
        return redirect(route('dashboard.help-categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->helpCategory->find($id);
        $categories = $this->helpCategory->pluck('title', 'id');

        return view('backend.help.help-category.edit', compact('categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HelpCategoryUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->helpCategory->update($request->validated(), $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            flash(__('alerts.backend.help.error'))->error();
            return redirect(route('dashboard.help-categories.index'));
        }

        flash(__('alerts.backend.help.help-category.updated'))->success();
        return redirect(route('dashboard.help-categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->helpCategory->delete($id);

        flash(__('alerts.backend.help.help-category.deleted'))->success();
        return redirect(route('dashboard.help-categories.index'));
    }
}
