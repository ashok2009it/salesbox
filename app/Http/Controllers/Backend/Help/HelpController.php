<?php

namespace App\Http\Controllers\Backend\Help;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Help\Help\HelpRepository;
use App\Repositories\Backend\Help\HelpCategory\HelpCategoryRepository;
use App\Http\Requests\Help\Help\HelpUpdateRequest;
use App\Http\Requests\Help\Help\HelpStoreRequest;
use Log;
use DB;

class HelpController extends Controller
{
    private $help;
    private $helpCategory;

    public function __construct(HelpRepository $help, HelpCategoryRepository $helpCategory)
    {
        $this->help = $help;
        $this->helpCategory = $helpCategory;
        $this->middleware('permission:view_helps', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_helps', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_helps', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_helps', ['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $helps = $this->help->all();

        return view('backend.help.help.index', compact('helps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->helpCategory->pluck('title', 'id');
        return view('backend.help.help.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HelpStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->help->create($request->validated());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            flash(__('alerts.backend.help.error'))->error();
            return redirect(route('dashboard.help.index'));
        }

        flash(__('alerts.backend.help.help.created'))->success();
        return redirect(route('dashboard.help.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $help = $this->help->find($id);

        return view('backend.help.help.show', compact('help'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $help = $this->help->find($id);
        $categories = $this->helpCategory->pluck('title', 'id');

        return view('backend.help.help.edit', compact('help', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HelpUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->help->update($request->validated(), $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::info($e);
            flash(__('alerts.backend.help.error'))->error();
            return redirect(route('dashboard.help.index'));
        }

        flash(__('alerts.backend.help.help.updated'))->success();
        return redirect(route('dashboard.help.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->help->delete($id);

        flash(__('alerts.backend.help.help.deleted'))->success();
        return redirect(route('dashboard.help.index'));
    }
}
