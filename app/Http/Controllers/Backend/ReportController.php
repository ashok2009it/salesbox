<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Report\ReportRepository;
use App\Http\Requests\Report\ReportRequest;
use DB;
use Log;

class ReportController extends Controller
{
    private $report;

    public function __construct(ReportRepository $report)
    {
        $this->middleware('permission:view_reports', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_reports', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_reports', ['only' => 'destroy']);
        $this->report = $report;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = $this->report->all();

        return view('backend.report.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = $this->report->find($id);

        return view('backend.report.show', compact('report'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = $this->report->find($id);

        return view('backend.report.edit', compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReportRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->report->update($request->validated(), $id);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            return redirect(route('dashboard.reports.index'));
        }

        flash(__('alerts.backend.report.updated'))->success();
        return redirect(route('dashboard.reports.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->report->delete($id);

        flash(__('alerts.backend.report.deleted'))->success();
        return redirect(route('dashboard.reports.index'));
    }
}
