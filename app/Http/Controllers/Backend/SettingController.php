<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Setting\SettingRepository;
use App\Http\Requests\Setting\SettingStoreRequest;
use App\Http\Requests\Setting\SettingUpdateRequest;
use Log;
use DB;

class SettingController extends Controller
{
    private $setting;

    public function __construct(SettingRepository $setting)
    {
        $this->setting = $setting;
        $this->middleware('permission:view_settings', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_settings', ['only' => ['edit', 'update', 'editSub']]);
        $this->middleware('permission:add_settings', ['only' => ['create', 'store', 'createSub']]);
        $this->middleware('permission:delete_settings', ['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = $this->setting->all();

        return view('backend.setting.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $setting = $this->setting->create($request->all());
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            flash(__('alerts.backend.setting.error'))->error();
            return redirect(route('dashboard.settings.index'));
        }
        flash(__('alerts.backend.setting.created'))->success();
        return redirect(route('dashboard.settings.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = $this->setting->find($id);

        return view('backend.setting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $setting = $this->setting->update($request->all(), $id);
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            flash(__('alerts.backend.setting.error'))->error();
            return redirect(route('dashboard.settings.index'));
        }
        flash(__('alerts.backend.setting.updated'))->success();
        return redirect(route('dashboard.settings.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->setting->delete($id);

        flash(__('alerts.backend.setting.deleted'))->success();
        return redirect(route('dashboard.settings.index'));
    }
}
