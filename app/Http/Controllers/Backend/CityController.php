<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\City\CityRepository;
use App\Http\Requests\City\CityStoreRequest;
use App\Http\Requests\City\CityUpdateRequest;

class CityController extends Controller
{
    private $city;
    private $category;

    public function __construct(CityRepository $city)
    {
        $this->middleware('permission:view_cities', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_cities', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_cities', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_cities', ['only' => 'destroy']);

        $this->city = $city;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = $this->city->all();

        return view('backend.city.index', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityStoreRequest $request)
    {
        $city = $this->city->create($request->validated());

        flash(__('alerts.backend.city.created'))->success();
        return redirect(route('dashboard.cities.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = $this->city->find($id);

        flash(__('alerts.backend.city.updated'))->info();
        return view('backend.city.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityUpdateRequest $request, $id)
    {
        $city = $this->city->update($request->validated(), $id);

        return redirect(route('dashboard.cities.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->city->delete($id);

        flash(__('alerts.backend.city.deleted'))->error();
        return redirect(route('dashboard.cities.index'));
    }
}
