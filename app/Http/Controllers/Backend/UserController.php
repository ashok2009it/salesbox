<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view_users', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_users', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_users', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_users', ['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()
                        ->search(request('search'))
                        ->statusFilter(request('status'))
                        ->paginate(10);

        return view('backend.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required|confirmed',
            'email' => 'email|required|unique:users'
        ]);

        $user = User::create([
            'name' => request('name'),
            'password' => bcrypt(request('password')),
            'email' => request('email'),
            'status' => 1
        ]);

        // if ($request->file('image')) {
        //     $user->addMedia($request->file('image'))
        //         ->toMediaCollection('avatar');
        // }

        if ($user) {
            flash("user created Successfully")->success();
        } else {
            flash("user creation error")->error();
        }

        return redirect(route('dashboard.users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = User::find($id);

        // return view('backend.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();

        return view('backend.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $this->validate($request, [
            'name' => 'required',
            'password' => 'required|confirmed',
            'email' => 'email|required|unique:users,email,'.$user->id
        ]);

        $user->update([
            'name' => request('name'),
            'password' => bcrypt(request('password')),
            'email' => request('email'),
            'status' => 1
        ]);

        if (request('password')) {
            $this->validate($request, [
                'password' => 'sometimes|required|string|min:6|confirmed',
            ]);
            $user->update([
                'password' => bcrypt(request('password')),
            ]);
        }
        
        // if ($request->file('image')) {
        //     $user->addMedia($request->file('image'))
        //         ->toMediaCollection('avatar');
        // }

        if ($user) {
            flash("user created Successfully")->success();
        } else {
            flash("user creation error")->error();
        }

        return redirect(route('dashboard.users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        
        flash("user delete Successfully")->success();
        
        return redirect(route('dashboard.users.index'));
    }

    public function assignRole(Request $request, $id)
    {
        $user = User::find($id);

        $user->syncRoles(request('role') ?? []);

        return redirect(route('dashboard.users.index'));
    }
}
