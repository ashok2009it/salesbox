<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view_dashboards', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_dashboards', ['only' => ['edit', 'update']]);
        $this->middleware('permission:add_dashboards', ['only' => ['create', 'store']]);
        $this->middleware('permission:delete_dashboards', ['only' => 'destroy']);
    }

    public function index()
    {
        return view('backend.index');
    }
}
