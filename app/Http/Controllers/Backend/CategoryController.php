<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Category\CategoryRepository;
use App\Http\Requests\Category\CategoryStoreRequest;
use App\Http\Requests\Category\CategoryUpdateRequest;
use DB;
use Log;

class CategoryController extends Controller
{
    private $category;

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
        $this->middleware('permission:view_categories', ['only' => ['index', 'show']]);
        $this->middleware('permission:edit_categories', ['only' => ['edit', 'update', 'editSub']]);
        $this->middleware('permission:add_categories', ['only' => ['create', 'store', 'createSub']]);
        $this->middleware('permission:delete_categories', ['only' => 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->all();

        return view('backend.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        global $categoriesList;
        $this->category->getTree();
        $types = $this->category->getType();

        return view('backend.category.create', compact('categoriesList', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $category = $this->category->create($request->all());
            $this->category->treeFix();
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            flash(__('alerts.backend.category.error'))->error();
            return redirect(route('dashboard.categories.index'));
        }
        flash(__('alerts.backend.category.created'))->success();
        return redirect(route('dashboard.categories.show', $category->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = $this->category->getChildren($id);
        
        return view('backend.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);

        global $categoriesList;
        $this->category->getTree();
        $types = $this->category->getType();

        return view('backend.category.edit', compact('category', 'categoriesList', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $category = $this->category->update($request->all(), $id);
            $this->category->treeFix();
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            flash(__('alerts.backend.category.error'))->error();
            return redirect(route('dashboard.categories.index'));
        }
        flash(__('alerts.backend.category.updated'))->success();
        return redirect(route('dashboard.categories.show', $category));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createSub($id)
    {
        global $categoriesList;
        $this->category->getTree();
        $types = $this->category->getType();
        $category = $this->category->find($id);
        $columns = $this->category->getColumns($category->attribute_type);

        return view('backend.category.subcategory.create', compact(
            'categoriesList',
            'columns',
            'category',
            'types'
        ));
    }

    public function editSub($id)
    {
        global $categoriesList;
        $this->category->getTree();
        $types = $this->category->getType();
        $category = $this->category->find($id);
        $columns = $this->category->getColumns($category->attribute_type);

        return view('backend.category.subcategory.edit', compact(
            'categoriesList',
            'columns',
            'category',
            'types'
        ));
    }
}
