<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginAsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasRole('admin')) {
            return redirect()->back();
        }
        Session::put('original_user', Auth::user()->id);
        $user = User::find($id);
        Auth::login($user);
        flash('Logged in as '.$user->name)
            ->success();

        return redirect(url('/'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $originalUserId = Session::pull('original_user');
        $originalUser = User::find($originalUserId);
        Auth::login($originalUser);
        flash('Logged back as '.$originalUser->name)
            ->success();

        return redirect()->back();
    }
}
