<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\SocialAccount;
use Socialite;
use Auth;

class SocialController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect(route('frontend.home'));
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = SocialAccount::where('provider_id', $user->id)->first();
        
        if ($authUser) {
            return $authUser->user();
        }

        $newUser = User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'status' => 1
        ]);

        SocialAccount::create([
            'user_id' => $newUser->id,
            'provider' => $provider,
            'provider_id' => $user->id,
        ]);

        return $newUser;
    }
}
