<?php

namespace App\Http\Requests\Ad;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'description' => 'nullable',
            'category_id' => 'required|integer',
            'city_id' => 'required|integer',
            'address' => 'nullable',
            'price' => 'nullable',
            'status' => 'nullable',
            'featured' => 'nullable',
            'attribute_type' => 'nullable',
        ];

        if (Request::input('attribute_type')) {
            $rules = array_merge($rules, $this->addRules(Request::input('attribute_type')));
        }
        return $rules;
    }

    private function addRules($attribute)
    {
        switch ($attribute) {
            case 'ElectronicAttribute':
                return [
                    'brand' => 'nullable',
                    'storage' => 'nullable',
                    'condition' => 'required'
                ];
            case 'JobAttribute':
                return [
                    'type' => 'required',
                    'position' => 'required',
                    'company_name' => 'nullable',
                    'experience_min' => 'nullable',
                    'experience_max' => 'nullable',
                    'salary_period' => 'nullable|integer',
                    'salary_min' => 'nullable',
                    'salary_max' => 'nullable'
                ];
            case 'PropertyAttribute':
                return [
                    'surface_size' => 'nullable',
                    'bedrooms' => 'nullable|integer',
                    'bathrooms' => 'nullable|integer',
                    'type_of_property' => 'nullable|integer',
                    'listed_by' => 'nullable|integer',
                    'parking' => 'nullable|integer',
                    'broker' => 'nullable|integer',
                    'furnished' => 'nullable|integer',
                    'pets' => 'nullable|integer'
                ];
            case 'VehicleAttribute':
                return [
                    'make' => 'nullable',
                    'type' => 'nullable',
                    'year' => 'nullable|integer',
                    'mileage' => 'nullable|integer',
                    'fuel' => 'nullable|integer',
                    'color' => 'nullable',
                    'first_owner' => 'nullable|integer',
                    'condition' => 'nullable|integer',
                    'transmission' => 'nullable|integer',
                    'listed_by' => 'nullable|integer',
                    'vin_number' => 'nullable'
                ];
            default:
                return [];
        }
    }
}
