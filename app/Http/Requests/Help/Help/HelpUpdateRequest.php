<?php

namespace App\Http\Requests\Help\Help;

use Illuminate\Foundation\Http\FormRequest;

class HelpUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'min:5|required|unique:helps,question,'.$this->help,
            'answer' => 'min:5|required',
            'status' => 'nullable',
            'category_id' => 'required|int',
            'popular' => 'nullable',
        ];
    }
}
