<?php

namespace App\Http\Requests\Help\HelpCategory;

use Illuminate\Foundation\Http\FormRequest;

class HelpCategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'min:3|required|unique:help_categories,title',
            'parent_id' => 'nullable|int'
        ];
    }
}
