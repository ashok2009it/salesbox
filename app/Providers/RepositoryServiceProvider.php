<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $repos = [
            'Category',
            'Ad',
            'City',
            'Setting'
        ];
        foreach ($repos as $repo) {
            $this->app->bind(
                "App\\Repositories\\Backend\\{$repo}\\{$repo}Interface",
                "App\\Repositories\\Backend\\{$repo}\\{$repo}Repository"
            );
        }

        $repos = [
            'ElectronicAttribute',
            'JobAttribute',
            'PropertyAttribute',
            'VehicleAttribute',
        ];
        foreach ($repos as $repo) {
            $this->app->bind(
                "App\\Repositories\\Backend\\Category\\{$repo}\\{$repo}Interface",
                "App\\Repositories\\Backend\\Category\\{$repo}\\{$repo}Repository"
            );
        }
    }
}
