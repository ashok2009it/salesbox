<?php

namespace App\Console\Commands;

use App\Models\Ad;
use Elasticsearch\Client;
use Illuminate\Console\Command;

class ReindexAds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:ads:rebuild';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindexes Ads';
    /**
     * @var \Elasticsearch\Client
     */
    private $client;
    /**
     * Create a new command instance.
     *
     * @param \Elasticsearch\Client $client
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Indexing all ads. Might take a while...');
        $mapping = [
            'index' => 'ads',
            'body' => [
                'settings' => [
                    'analysis' => [
                        'analyzer' => [
                            'my_analyzer' => [
                                'tokenizer' => 'my_tokenizer',
                                'filter' => [
                                    'lowercase'
                                ]
                            ]
                        ],
                        'tokenizer' => [
                            'my_tokenizer' => [
                                'type' => 'ngram',
                                'min_gram' => 3,
                                'max_gram' => 3,
                                'token_chars' => [
                                    'letter',
                                    'digit'
                                ]
                            ]
                        ]
                    ]
                ],

                'mappings' => [
                    'ads' => [
                        '_source' => [
                            'enabled' => true
                        ],
                        'properties' => [
                            'title' => [
                                'type' => 'text',
                                'analyzer' => 'my_analyzer'
                            ],
                        ]
                    ]
                ]
            ]
        ];

        $this->client->indices()->create($mapping);
        /** @var Ad $ad */
        foreach (Ad::cursor() as $ad) {
            $this->client->index([
                'index' => $ad->getSearchIndex(),
                'type' => $ad->getSearchType(),
                'id' => $ad->id,
                'body' => $ad->toSearchArray(),
            ]);
            // PHPUnit-style feedback
            $this->output->write('.');
        }
        $this->info("\nDone!");
    }
}
