<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;

class HelpCategory extends Model
{
    protected $fillable = [
        'title',
        'parent_id',
        'slug'
    ];

    public function scopeChildrenCategory($query, $parent)
    {
        if ($parent) {
            return $query->where('parent_id', $parent);
        }

        return $query;
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function help()
    {
        return $this->hasMany(Help::class, 'category_id');
    }
}
