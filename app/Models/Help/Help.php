<?php

namespace App\Models\Help;

use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    protected $fillable = [
        'question',
        'answer',
        'popular',
        'status',
        'category_id',
        'slug',
        'status'
    ];

    const STATUS = [
        'Off',
        'On'
    ];
    
    public function category()
    {
        return $this->belongsTo(HelpCategory::class, 'category_id');
    }

    public function scopeSearch($query, $search)
    {
        if ($search) {
            return $query->where('question', 'like', '%'.$search.'%');
        }

        return $query;
    }
}
