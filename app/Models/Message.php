<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Message extends Model
{
    protected $fillable = [
        'user_id',
        'customer_id',
        'message'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function reciever()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }
}
