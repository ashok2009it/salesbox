<?php

namespace App\Models;

use App\Models\Ad;
use App\Models\SocialAccount;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status',
        'last_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'like', '%' . $search . '%')
                     ->orWhere('id', 'like', '%' . $search . '%')
                     ->orWhere('email', 'like', '%' . $search . '%');
    }

    public function scopeStatusFilter($query, $filter)
    {
        if ($filter) {
            return $query->where('status', $filter == 'Active' ? 1 : 0);
        }
        return $query;
    }

    public function socialAccount()
    {
        return $this->hasOne(SocialAccount::class);
    }

    public function delete()
    {
        $this->socialAccounts()->delete();

        return parent::delete();
    }

    public function customer()
    {
        return $this->belongsToMany('App\Models\User', 'messages', 'user_id', 'customer_id');
    }

    public function seller()
    {
        return $this->belongsToMany('App\Models\User', 'messages', 'customer_id', 'user_id');
    }

    public function messages()
    {
        return $this->customer->merge($this->seller);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class, 'created_by');
    }
}
