<?php

namespace App\Models;

use App\Search\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use Searchable;
    use SoftDeletes;
    
    protected $fillable = [
        'title',
        'slug',
        'price',
        'category_id',
        'description',
        'status',
        'created_by',
        'updated_by',
        'address',
        'city_id',
        'featured',
        'attribute_type'
    ];

    protected $dates = ['deleted_at'];

    const STATUS = [
        'InActive',
        'Active',
        'Rejected'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by')->withDefault();
    }

    public function scopeSearch($query, $search)
    {
        if ($search) {
            return $query->where('title', 'like', '%'.$search.'%');
        }

        return $query;
    }

    public function scopeFilter($query, $filter)
    {
        if ($filter && in_array($filter, self::STATUS)) {
            return $query->where('status', array_flip(self::STATUS)[$filter]);
        }

        return $query;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function attributes()
    {
        if ($this->attribute_type) {
            return $this->hasOne("App\Models\Category\\".$this->attribute_type);
        } else {
            return $this->hasOne("App\Models\Category\VehicleAttribute")->withDefault();
        }
    }
}
