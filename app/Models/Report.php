<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'user_id',
        'ad_id',
        'status',
        'message',
    ];

    const STATUS = [
        'Pending',
        'Completed'
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class)->withDefault();
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function scopeFilter($query, $filter)
    {
        if ($filter && in_array($filter, self::STATUS)) {
            return $query->where('status', array_flip(self::STATUS)[$filter]);
        }

        return $query;
    }
}
