<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ad;

class ElectronicAttribute extends Model
{
    protected $fillable = [
        'brand',
        'storage',
        'condition',
        'ad_id',
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }
}
