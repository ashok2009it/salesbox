<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ad;

class VehicleAttribute extends Model
{
    protected $fillable = [
        'make',
        'type',
        'year',
        'mileage',
        'fuel',
        'color',
        'first_owner',//yes or no
        'condition',
        'transmission',
        'listed_by',
        'vin_number',
        'ad_id',
    ];

    const FUEL = [
        'petrol',
        'diesel',
        'other'
    ];

    const LISTED_BY = [
        'Agent',
        'Owner',
    ];

    const CONDITION = [
        'Used',
        'New',
    ];

    const FIRST_OWNER = [
        'No',
        'Yes',
    ];

    const TRANSMISSION = [
        'Manual',
        'Automatic',
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }
}
