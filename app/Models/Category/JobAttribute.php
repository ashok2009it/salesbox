<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ad;

class JobAttribute extends Model
{
    protected $fillable = [
        'type',
        'position',
        'company_name',
        'experience_min',
        'experience_max',
        'salary_period',
        'salary_min',
        'salary_max',
        'ad_id',
    ];

    const TYPE = [
        'Seeker',
        'Employer',
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }
}
