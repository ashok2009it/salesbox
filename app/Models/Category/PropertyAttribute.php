<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ad;

class PropertyAttribute extends Model
{
    protected $fillable = [
        'surface_size',
        'bedrooms',
        'bathrooms',
        'type_of_property',
        'listed_by',
        'parking',
        'broker',//yes or no
        'furnished',
        'pets',//allowed or not
        'ad_id',
    ];

    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }
}
