<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use App\Models\Category\ElectronicAttribute;
use App\Models\Category\JobAttribute;
use App\Models\Category\PropertyAttribute;
use App\Models\Category\VehicleAttribute;

class Category extends Model
{
    use NodeTrait;

    protected $fillable = [
        'title',
        'slug',
        'icon',
        'attribute_type',
        'attributes',
        'status'
    ];

    const TYPE = [
        'JobAttribute',
        'ElectronicAttribute',
        'VehicleAttribute',
        'PropertyAttribute'
    ];
    
    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    public function columns($class)
    {
        return (new $class)->getFillable();
    }
}
