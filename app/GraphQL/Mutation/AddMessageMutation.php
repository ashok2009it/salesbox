<?php

namespace App\GraphQL\Mutation;

use GraphQL;
use App\Models\Message;
use App\Events\MessageSent;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class AddMessageMutation extends Mutation
{
    protected $attributes = [
        'name' => 'addMessage'
    ];

    public function type()
    {
        return GraphQL::type('message');
    }

    public function args()
    {
        return [
            'message' => ['name' => 'message', 'type' => Type::nonNull(Type::string())],
            'sender' => ['name' => 'sender', 'type' => Type::nonNull(Type::int())],
            'reciever' => ['name' => 'reciever', 'type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args)
    {
        $message = Message::create([
            'message' => $args['message'],
            'user_id' => $args['sender'],
            'customer_id' => $args['reciever'],
        ]);
        
        broadcast(new MessageSent($message->reciever, $message));
        return $message;
    }
}
