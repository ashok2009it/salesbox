<?php

namespace App\GraphQL\Type;

use App\Models\Ad;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class AdType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Ad',
        'description'   => 'An Ad',
        'model'         => Ad::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of ad',
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of ad',
            ],
            'price' => [
                'type' => Type::string(),
                'description' => 'The price of ad',
            ],
            'category_id' => [
                'type' => Type::int(),
                'description' => 'The category_id of ad',
            ],
            'category' => [
                'type' => GraphQL::type('category'),
                'description' => 'The category of ad',
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'The description of ad',
            ],
            'createdBy' => [
                'type' => GraphQL::type('user'),
                'description' => 'creator of ad',
            ],
            'updatedBy' => [
                'type' => GraphQL::type('user'),
                'description' => 'last updator of ad',
            ],
            'address' => [
                'type' => Type::string(),
                'description' => 'Address of the creator',
            ],
            'city' => [
                'type' => GraphQL::type('city'),
                'description' => 'city of the creator',
            ],
            'featured' => [
                'type' => Type::boolean(),
                'description' => 'is ad featured?',
            ],
            'status' => [
                'type' => Type::int(),
                'description' => 'The status of ad',
            ],
            'attribute_type' => [
                'type' => Type::string(),
                'description' => 'The status of ad',
            ],
            'attributes' => [
                'type' => GraphQL::type($this->attribute_type ? lcfirst($this->attribute_type) : 'vehicleAttribute'),
                'description' => 'The attributes of ad',
            ],
            'attribute' => [
                'type' => Type::string(),
                'description' => 'The status of ad',
            ],
        ];
    }

    public function resolveAttributeField($root, $args)
    {
        return json_encode($root->attributes);
    }
}
