<?php

namespace App\GraphQL\Type;

use App\Models\Category;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CategoryType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Category',
        'description'   => 'A category',
        'model'         => Category::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of category',
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'The title of category',
            ],
            'slug' => [
                'type' => Type::string(),
                'description' => 'The slug of category',
            ],
            'icon' => [
                'type' => Type::string(),
                'description' => 'The icon of category',
            ],
            'attribute_type' => [
                'type' => Type::string(),
                'description' => 'The attribute type of category',
            ],
            'attributes' => [
                'type' => Type::listOf(Type::string()),
                'description' => 'The attributes of category',
            ],
            'status' => [
                'type' => Type::int(),
                'description' => 'The status of category',
            ],
        ];
    }

    public function resolveAttributesField($root, $args)
    {
        return json_decode($root->attributes);
    }
}
