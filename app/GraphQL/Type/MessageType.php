<?php

namespace App\GraphQL\Type;

use GraphQL;
use App\Models\Message;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MessageType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Message',
        'description'   => 'A Message',
        'model'         => Message::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of Message',
            ],
            'user_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The user_id of Message',
            ],
            'customer_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The customer_id of Message',
            ],
            'sender' => [ //sender
                'type' => GraphQL::type('user'),
                'description' => 'The message sender',
            ],
            'reciever' => [ //reciever
                'type' => GraphQL::type('user'),
                'description' => 'The message sender',
            ],
            'message' => [
                'type' => Type::string(),
                'description' => 'Message'
            ]
        ];
    }
}
