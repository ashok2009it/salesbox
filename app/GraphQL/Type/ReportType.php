<?php

namespace App\GraphQL\Type;

use GraphQL;
use App\Models\Report;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ReportType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Report',
        'description'   => 'A Report',
        'model'         => Report::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of report',
            ],
            'message' => [
                'type' => Type::string(),
                'description' => 'The message of report',
            ],
            'status' => [
                'type' => Type::int(),
                'description' => 'The message of report',
            ],
            'ad' => [
                'type' => GraphQL::type('ad'),
                'description' => 'The reported ad'
            ],
            'user' => [
                'type' => GraphQL::type('user'),
                'description' => 'The creator of report'
            ]
        ];
    }
}
