<?php

namespace App\GraphQL\Type;

use App\Models\City;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CityType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'City',
        'description'   => 'A city',
        'model'         => City::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of city',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of city',
            ],
        ];
    }
}
