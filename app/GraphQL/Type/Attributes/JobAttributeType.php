<?php

namespace App\GraphQL\Type\Attributes;

use App\Models\Category\JobAttribute;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class JobAttributeType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'JobAttribute',
        'description'   => 'A JobAttribute',
        'model'         => JobAttribute::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of job',
            ],
            'type' => [
                'type' => Type::int(),
                'description' => 'The type of job',
            ],
            'position' => [
                'type' => Type::string(),
                'description' => 'The position of job',
            ],
            'company_name' => [
                'type' => Type::string(),
                'description' => 'The company_name of job',
            ],
            'experience_min' => [
                'type' => Type::string(),
                'description' => 'The min experience of job',
            ],
            'experience_max' => [
                'type' => Type::string(),
                'description' => 'The max experience of job',
            ],
            'salary_period' => [ // hourly, weekly
                'type' => Type::string(),
                'description' => 'The salary period of job',
            ],
            'salary_min' => [
                'type' => Type::string(),
                'description' => 'The min salary of job',
            ],
            'salary_max' => [
                'type' => Type::string(),
                'description' => 'The max salary of job',
            ],
        ];
    }
}
