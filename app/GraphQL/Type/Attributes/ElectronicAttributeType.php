<?php

namespace App\GraphQL\Type\Attributes;

use App\Models\Category\ElectronicAttribute;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ElectronicAttributeType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'ElectronicAttribute',
        'description'   => 'A ElectronicAttribute',
        'model'         => ElectronicAttribute::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of electronic',
            ],
            'brand' => [
                'type' => Type::string(),
                'description' => 'The brand of electronic',
            ],
            'storage' => [
                'type' => Type::string(),
                'description' => 'The storage of electronic',
            ],
        ];
    }
}
