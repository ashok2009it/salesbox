<?php

namespace App\GraphQL\Type\Attributes;

use App\Models\Category\VehicleAttribute;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class VehicleAttributeType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'VehicleAttribute',
        'description'   => 'A VehicleAttribute',
        'model'         => VehicleAttribute::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of vehicle',
            ],
            'make' => [
                'type' => Type::string(),
                'description' => 'The make of vehicle',
            ],
            'type' => [
                'type' => Type::string(),
                'description' => 'The type of vehicle',
            ],
            'year' => [
                'type' => Type::int(),
                'description' => 'The year vehicle was made',
            ],
            'mileage' => [
                'type' => Type::int(),
                'description' => 'The mileage of vehicle',
            ],
            'fuel' => [
                'type' => Type::int(),
                'description' => 'The fuel type of vehicle',
            ],
            'color' => [
                'type' => Type::string(),
                'description' => 'The color of vehicle',
            ],
            'first_owner' => [
                'type' => Type::boolean(),
                'description' => 'The first owner of vehicle',
            ],
            'condition' => [
                'type' => Type::boolean(),
                'description' => 'The condition of vehicle',
            ],
            'transmission' => [
                'type' => Type::boolean(),
                'description' => 'The transmission of vehicle',
            ],
            'listed_by' => [
                'type' => Type::boolean(),
                'description' => 'vehicle was listed by',
            ],
            'vin_number' => [
                'type' => Type::boolean(),
                'description' => 'The vin number of vehicle',
            ],
        ];
    }
}
