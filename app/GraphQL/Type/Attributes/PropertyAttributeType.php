<?php

namespace App\GraphQL\Type\Attributes;

use App\Models\Category\PropertyAttribute;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class PropertyAttributeType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'PropertyAttribute',
        'description'   => 'A PropertyAttribute',
        'model'         => PropertyAttribute::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of property',
            ],
            'surface_size' => [
                'type' => Type::string(),
                'description' => 'The surface_size of property',
            ],
            'bedrooms' => [
                'type' => Type::int(),
                'description' => 'The bedrooms in property',
            ],
            'bathrooms' => [
                'type' => Type::int(),
                'description' => 'The bathrooms in property',
            ],
            'type_of_property' => [
                'type' => Type::int(),
                'description' => 'The type of property',
            ],
            'listed_by' => [
                'type' => Type::int(),
                'description' => 'property was listed by',
            ],
            'parking' => [
                'type' => Type::boolean(),
                'description' => 'The parking in property',
            ],
            'broker' => [
                'type' => Type::boolean(),
                'description' => 'The broker in property',
            ],
            'furnished' => [
                'type' => Type::boolean(),
                'description' => 'Is property furnished?',
            ],
            'pets' => [
                'type' => Type::boolean(),
                'description' => 'Are pets allowed in property',
            ],
        ];
    }
}
