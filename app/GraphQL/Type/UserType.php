<?php

namespace App\GraphQL\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use App\Models\User;
use GraphQL;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'User',
        'description'   => 'A User',
        'model'         => User::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of User',
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of User',
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'The email of User',
            ],
            'status' => [
                'type' => Type::string(),
                'description' => 'The status of User',
            ],
            'socialAccount' => [
                'type' => GraphQL::type('socialAccount'),
                'description' => 'SocialAccount of user'
            ],
        ];
    }
}
