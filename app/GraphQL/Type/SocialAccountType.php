<?php

namespace App\GraphQL\Type;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use App\Models\SocialAccount;
use GraphQL;

class SocialAccountType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'SocialAccount',
        'description'   => 'A SocialAccount',
        'model'         => SocialAccount::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of SocialAccount',
            ],
            'provider' => [
                'type' => Type::string(),
                'description' => 'The provider of SocialAccount',
            ],
            'provider_id' => [
                'type' => Type::string(),
                'description' => 'The provider id of SocialAccount',
            ],
            'user' => [
                'type' => GraphQL::type('user'),
                'description' => 'The user of SocialAccount',
            ],
        ];
    }
}
