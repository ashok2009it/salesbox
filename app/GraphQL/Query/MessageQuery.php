<?php

namespace App\GraphQL\Query;

use App\Models\Message;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class MessageQuery extends Query
{
    protected $attributes = [
        'name' => 'Message query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('message'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
            'sender' => ['name' => 'sender', 'type' => Type::int()],
            'reciever' => ['name' => 'reciever', 'type' => Type::int()],
        ];
    }

    public function resolve($root, $args)
    {
        $query = Message::query();
        
        if (isset($args['id'])) {
            $query->where('id', $args['id']);
        }
        if (isset($args['sender']) && isset($args['reciever'])) {
            $query->where([
                ['user_id', $args['sender']],
                ['customer_id', $args['reciever']]
            ])
            ->orWhere([
                ['user_id', $args['reciever']],
                ['customer_id', $args['sender']]
            ]);
        } else {
            if (isset($args['sender'])) {
                $query->where('user_id', $args['sender']);
            }
            if (isset($args['reciever'])) {
                $query->where('customer_id', $args['reciever']);
            }
        }

        return $query->get();
    }
}
