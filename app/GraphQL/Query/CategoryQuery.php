<?php

namespace App\GraphQL\Query;

use App\Models\Category;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class CategoryQuery extends Query
{
    protected $attributes = [
        'name' => 'Category query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('category'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'parent' => ['name' => 'parent', 'type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return Category::where('id', $args['id'])->get();
        }

        if (isset($args['parent'])) {
            if ($args['parent'] == 'children') {
                return Category::where('parent', '!=', null)->get();
            } else {
                return Category::where('parent', null)->get();
            }
        }

        return Category::all();
    }
}
