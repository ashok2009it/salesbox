<?php

namespace App\GraphQL\Query;

use App\Models\City;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class CityQuery extends Query
{
    protected $attributes = [
        'name' => 'City query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('city'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'name' => ['name' => 'name', 'type' => Type::string()]
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return City::where('id', $args['id'])->get();
        }

        if (isset($args['name'])) {
            return City::where('name', 'like', '%'.$args['name'].'%')->get();
        }

        return City::all();
    }
}
