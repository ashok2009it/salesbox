<?php

namespace App\GraphQL\Query;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use App\Models\Ad;
use GraphQL;

class AdQuery extends Query
{
    protected $attributes = [
        'name' => 'Ad query'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('ad'));
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::int()],
        ];
    }

    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return Ad::where('id', $args['id'])->get();
        }

        return Ad::with('attributes')->get();
    }
}
