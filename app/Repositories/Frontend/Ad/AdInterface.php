<?php

namespace App\Repositories\Frontend\Ad;

use Illuminate\Database\Eloquent\Collection;

interface AdInterface
{
    public function search(string $query = ""): Collection;
}
