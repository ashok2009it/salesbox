<?php

namespace App\Repositories\Backend\City;

interface CityInterface
{
    public function all();

    public function create(array $attributes);

    public function update(array $attributes, $id);

    public function delete($id);

    public function find($id);
}
