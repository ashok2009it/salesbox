<?php

namespace App\Repositories\Backend\City;

use Illuminate\Database\Eloquent\Model;
use App\Models\City;

class CityRepository implements CityInterface
{
    private $cityModel;

    public function __construct(City $cityModel)
    {
        $this->cityModel = $cityModel;
    }

    public function all()
    {
        return $this->cityModel->latest()
                    ->search(request('search'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->cityModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->cityModel->create([
            'name' => $attributes['name']
        ]);
    }

    public function update(array $attributes, $id)
    {
        $city = $this->cityModel->find($id);

        return $city->update([
            'name' => $attributes['name'],
        ]);
    }

    public function delete($id)
    {
        return $this->cityModel->find($id)->delete();
    }

    public function pluck($index, $key)
    {
        return $this->cityModel->all()->pluck($index, $key);
    }
}
