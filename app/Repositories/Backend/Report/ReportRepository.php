<?php

namespace App\Repositories\Backend\Report;

use App\Models\Report;
use Illuminate\Database\Eloquent\Model;

class ReportRepository implements ReportInterface
{
    private $reportModel;

    public function __construct(Report $reportModel)
    {
        $this->reportModel = $reportModel;
    }

    public function all()
    {
        return $this->reportModel
                    ->latest()
                    ->filter(request('status'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->reportModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        $dataAttributs =
            [
                'message' => $attributes['message'],
                'user_id' => $attributes['user_id'],
                'ad_id' => $attributes['ad_id'],
                'status' => $attributes['status'] ?? 0
            ];

        return $this->reportModel->create($dataAttributs);
    }

    public function update(array $attributes, $id)
    {
        $dataAttributs =
            [
                'message' => $attributes['message'],
                'status' => $attributes['status'] ?? 0,
            ];

        $report = $this->find($id);
        return tap($report)->update($dataAttributs);
    }

    public function delete($id)
    {
        return $this->reportModel->find($id)->delete();
    }
}
