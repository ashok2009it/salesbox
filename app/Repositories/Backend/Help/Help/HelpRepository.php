<?php

namespace App\Repositories\Backend\Help\Help;

use Illuminate\Database\Eloquent\Model;
use App\Models\Help\Help;

class HelpRepository implements HelpInterface
{
    private $helpModel;

    public function __construct(Help $helpModel)
    {
        $this->helpModel = $helpModel;
    }

    public function all()
    {
        return $this->helpModel->latest()
                    ->search(request('search'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->helpModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->helpModel->create([
            'question' => $attributes['question'],
            'answer' => $attributes['answer'],
            'category_id' => $attributes['category_id'],
            'slug' => $this->setSlugAttribute($attributes['question']),
            'status' => $attributes['status'] ?? null,
            'popular' => $attributes['popular'] ?? 0
        ]);
    }

    public function update(array $attributes, $id)
    {
        $help = $this->helpModel->find($id);

        return $help->update([
            'question' => $attributes['question'],
            'answer' => $attributes['answer'],
            'category_id' => $attributes['category_id'],
            'status' => $attributes['status'] ?? null,
            'popular' => $attributes['popular'] ?? 0
        ]);
    }

    public function delete($id)
    {
        return $this->helpModel->find($id)->delete();
    }

    private function setSlugAttribute($slug)
    {
        $slug = str_slug($slug);
        $slugs = $this->helpModel->whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
            ->orderBy('id')
            ->pluck('slug');
        if (count($slugs) === 0) {
            return $slug;
        } elseif (!$slugs->isEmpty()) {
            $pieces = explode('-', $slugs);
            $number = (int)end($pieces);
            return $slug .= '-' . ($number + 1);
        }
    }
}
