<?php

namespace App\Repositories\Backend\Help\Help;

interface HelpInterface
{
    public function all();

    public function create(array $attributes);

    public function update(array $attributes, $id);

    public function delete($id);

    public function find($id);
}
