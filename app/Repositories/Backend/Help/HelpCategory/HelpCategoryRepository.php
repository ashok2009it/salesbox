<?php

namespace App\Repositories\Backend\Help\HelpCategory;

use Illuminate\Database\Eloquent\Model;
use App\Models\Help\HelpCategory;

class HelpCategoryRepository implements HelpCategoryInterface
{
    private $helpCategoryModel;

    public function __construct(HelpCategory $helpCategoryModel)
    {
        $this->helpCategoryModel = $helpCategoryModel;
    }

    public function all()
    {
        return $this->helpCategoryModel->latest()
                    ->childrenCategory(request('parent'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->helpCategoryModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->helpCategoryModel->create([
            'title' => $attributes['title'],
            'slug' => str_slug($attributes['title'], '-'),
            'parent_id' => $attributes['parent_id'] ?? null
        ]);
    }

    public function update(array $attributes, $id)
    {
        $helpCategory = $this->helpCategoryModel->find($id);

        return $helpCategory->update([
            'title' => $attributes['title'],
            'parent_id' => $attributes['parent_id'] ?? null
        ]);
    }

    public function delete($id)
    {
        return $this->helpCategoryModel->find($id)->delete();
    }

    public function pluck($index, $key)
    {
        return $this->helpCategoryModel->all()->pluck($index, $key);
    }
}
