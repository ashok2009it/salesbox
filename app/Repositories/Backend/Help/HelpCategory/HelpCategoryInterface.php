<?php

namespace App\Repositories\Backend\Help\HelpCategory;

interface HelpCategoryInterface
{
    public function all();

    public function create(array $attributes);

    public function update(array $attributes, $id);

    public function delete($id);

    public function find($id);
}
