<?php

namespace App\Repositories\Backend\Category\ElectronicAttribute;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category\ElectronicAttribute;

class ElectronicAttributeRepository implements ElectronicAttributeInterface
{
    private $electronicAttributeModel;

    public function __construct(ElectronicAttribute $electronicAttributeModel)
    {
        $this->electronicAttributeModel = $electronicAttributeModel;
    }

    public function all()
    {
        return $this->electronicAttributeModel->latest()
                    ->search(request('search'))
                    ->filter(request('filter'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->electronicAttributeModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->electronicAttributeModel->create([
            'brand' => $attributes['brand'],
            'storage' => $attributes['storage'],
            'condition' => $attributes['condition'] ?? 0,
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function update(array $attributes, $id)
    {
        $electronicAttribute = $this->electronicAttributeModel->find($id);

        return $electronicAttribute->update([
            'brand' => $attributes['brand'],
            'storage' => $attributes['storage'],
            'condition' => $attributes['condition'] ?? 0,
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function delete($id)
    {
        return $this->electronicAttributeModel->find($id)->delete();
    }
}
