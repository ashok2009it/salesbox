<?php

namespace App\Repositories\Backend\Category\JobAttribute;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category\JobAttribute;

class JobAttributeRepository implements JobAttributeInterface
{
    private $jobAttributeModel;

    public function __construct(JobAttribute $jobAttributeModel)
    {
        $this->jobAttributeModel = $jobAttributeModel;
    }

    public function all()
    {
        return $this->jobAttributeModel->latest()
                    ->search(request('search'))
                    ->filter(request('filter'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->jobAttributeModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->jobAttributeModel->create([
            'type' => $attributes['type'],
            'company_name' => $attributes['company_name'],
            'position' => $attributes['position'],
            'experience_min' => $attributes['experience_min'] ?? 0,
            'experience_max' => $attributes['experience_max'] ?? 0,
            'salary_period' => $attributes['salary_period'],
            'salary_min' => $attributes['salary_min'],
            'salary_max' => $attributes['salary_max'],
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function update(array $attributes, $id)
    {
        $jobAttribute = $this->jobAttributeModel->find($id);

        return $jobAttribute->update([
            'type' => $attributes['type'],
            'company_name' => $attributes['company_name'],
            'position' => $attributes['position'],
            'experience_min' => $attributes['experience_min'] ?? 0,
            'experience_max' => $attributes['experience_max'] ?? 0,
            'salary_period' => $attributes['salary_period'],
            'salary_min' => $attributes['salary_min'],
            'salary_max' => $attributes['salary_max'],
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function delete($id)
    {
        return $this->jobAttributeModel->find($id)->delete();
    }
}
