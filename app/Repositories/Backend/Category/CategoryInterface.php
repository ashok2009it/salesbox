<?php

namespace App\Repositories\Backend\Category;

interface CategoryInterface
{
    public function all();

    public function create(array $attributes);

    public function update(array $attributes, $id);

    public function delete($id);

    public function find($id);
    
    public function getTree();

    public function getChildren($id);

    public function treeFix();
}
