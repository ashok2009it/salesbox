<?php

namespace App\Repositories\Backend\Category\PropertyAttribute;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category\PropertyAttribute;

class PropertyAttributeRepository implements PropertyAttributeInterface
{
    private $propertyAttributeModel;

    public function __construct(PropertyAttribute $propertyAttributeModel)
    {
        $this->propertyAttributeModel = $propertyAttributeModel;
    }

    public function all()
    {
        return $this->propertyAttributeModel->latest()
                    ->search(request('search'))
                    ->filter(request('filter'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->propertyAttributeModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->propertyAttributeModel->create([
            'surface_size' => $attributes['surface_size'],
            'bedrooms' => $attributes['bedrooms'],
            'bathrooms' => $attributes['bathrooms'],
            'type_of_property' => $attributes['type_of_property'],
            'listed_by' => $attributes['listed_by'] ?? null,
            'parking' => $attributes['parking'] ?? null,
            'broker' => $attributes['broker'] ?? null,
            'furnished' => $attributes['furnished'] ?? null,
            'pets' => $attributes['pets'] ?? null,
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function update(array $attributes, $id)
    {
        $propertyAttribute = $this->propertyAttributeModel->find($id);

        return $propertyAttribute->update([
            'surface_size' => $attributes['surface_size'],
            'bedrooms' => $attributes['bedrooms'],
            'bathrooms' => $attributes['bathrooms'],
            'type_of_property' => $attributes['type_of_property'],
            'listed_by' => $attributes['listed_by'] ?? null,
            'parking' => $attributes['parking'] ?? null,
            'broker' => $attributes['broker'] ?? null,
            'furnished' => $attributes['furnished'] ?? null,
            'pets' => $attributes['pets'] ?? null,
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function delete($id)
    {
        return $this->propertyAttributeModel->find($id)->delete();
    }
}
