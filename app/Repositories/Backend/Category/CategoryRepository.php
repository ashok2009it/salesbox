<?php

namespace App\Repositories\Backend\Category;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryRepository implements CategoryInterface
{
    private $categoryModel;

    public function __construct(Category $categoryModel)
    {
        $this->categoryModel = $categoryModel;
    }

    public function all()
    {
        $this->treeFix();

        if (!empty(request('sub_cat'))) {
            $parent_id = request('sub_cat');
            return $this->categoryModel->where('parent_id', $parent_id)
                        ->latest()
                        ->paginate(10);
        } else {
            return $this->categoryModel->where('parent_id', null)
                        ->latest()
                        ->paginate(10);
        }
    }

    public function find($id)
    {
        return $this->categoryModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        if ($attributes['attributes'] ?? null) {
            $columnAttributes = json_encode($attributes['attributes']);
        } else {
            $columnAttributes = null;
        }
        $dataAttributs =
            [
                'title' => title_case(request('title')),
                'slug' => str_slug(request('title'), '-'),
                'icon' => request('icon'),
                'status' => request('status') ?? 0,
                'parent_id' => $attributes['category_id'] ?? null,
                'attribute_type' => $this->getSingleType($attributes['attribute_type'] ?? null),
                'attributes' => $columnAttributes
            ];

        if (isset($attributes['category_id']) && $attributes['category_id'] > 0) {
            $parent = $this->find($attributes['category_id']);
            if ($parent) {
                return $parent->children()->create($dataAttributs);
            }
        } else {
            return $this->categoryModel->create($dataAttributs);
        }
    }

    public function update(array $attributes, $id)
    {
        if ($attributes['attributes'] ?? null) {
            $columnAttributes = json_encode($attributes['attributes']);
        } else {
            $columnAttributes = null;
        }

        $dataAttributs =
            [
                'title' => title_case(request('title')),
                'icon' => request('icon'),
                'status' => request('status') ?? 0,
                'parent_id' => $attributes['category_id'],
                'attributes' => $columnAttributes
            ];

        $category = $this->find($id);
        return tap($category)->update($dataAttributs);
    }

    public function delete($id)
    {
        //
    }

    public function getTree()
    {
        return $this->catDiff($this->categoryModel->get()->toTree());
    }

    public function getChildren($id)
    {
        return $this->categoryModel->descendantsAndSelf($id)->toTree()->first();
    }

    public function getSubCategory()
    {
        return $this->categoryModel->where('parent_id', '!=', null)->get();
    }

    public function treeFix()
    {
        return $this->categoryModel->fixTree();
    }

    public function getType()
    {
        return $this->categoryModel::TYPE;
    }

    public function getColumns($class)
    {
        if ($class) {
            return $this->categoryModel->columns('App\Models\Category\\'.$class);
        } else {
            return null;
        }
    }

    private function catDiff($nodes)
    {
        $traverse = function ($categories, $prefix = '-') use (&$traverse) {
            global $categoriesList;
            foreach ($categories as $category) {
                $categoriesList[$category->id] = $prefix . ' ' . $category->title;
                $traverse($category->children, $prefix . '-');
            }
        };
        $traverse($nodes);
    }

    private function getSingleType($type)
    {
        if ($type != null) {
            return $this->getType()[$type];
        }
        return null;
    }
}
