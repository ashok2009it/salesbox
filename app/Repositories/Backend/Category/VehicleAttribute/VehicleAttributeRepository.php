<?php

namespace App\Repositories\Backend\Category\VehicleAttribute;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category\VehicleAttribute;

class VehicleAttributeRepository implements VehicleAttributeInterface
{
    private $vehicleAttributeModel;

    public function __construct(VehicleAttribute $vehicleAttributeModel)
    {
        $this->vehicleAttributeModel = $vehicleAttributeModel;
    }

    public function all()
    {
        return $this->vehicleAttributeModel->latest()
                    ->search(request('search'))
                    ->filter(request('filter'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->vehicleAttributeModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        return $this->vehicleAttributeModel->create([
            'make' => $attributes['make'],
            'type' => $attributes['type'],
            'year' => $attributes['year'],
            'mileage' => $attributes['mileage'],
            'fuel' => $attributes['fuel'],
            'color' => $attributes['color'],
            'first_owner' => $attributes['first_owner'] ?? null,
            'condition' => $attributes['condition'] ?? null,
            'transmission' => $attributes['transmission'] ?? null,
            'listed_by' => $attributes['listed_by'] ?? null,
            'vin_number' => $attributes['vin_number'],
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function update(array $attributes, $id)
    {
        $vehicleAttribute = $this->vehicleAttributeModel->find($id);

        return $vehicleAttribute->update([
            'make' => $attributes['make'],
            'type' => $attributes['type'],
            'year' => $attributes['year'],
            'mileage' => $attributes['mileage'],
            'fuel' => $attributes['fuel'],
            'color' => $attributes['color'],
            'first_owner' => $attributes['first_owner'] ?? null,
            'condition' => $attributes['condition'] ?? null,
            'transmission' => $attributes['transmission'] ?? null,
            'listed_by' => $attributes['listed_by'] ?? null,
            'vin_number' => $attributes['vin_number'],
            'ad_id' => $attributes['ad_id'],
        ]);
    }

    public function delete($id)
    {
        return $this->vehicleAttributeModel->find($id)->delete();
    }
}
