<?php

namespace App\Repositories\Backend\Ad;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\Category\ElectronicAttribute\ElectronicAttributeRepository;
use App\Repositories\Backend\Category\JobAttribute\JobAttributeRepository;
use App\Repositories\Backend\Category\PropertyAttribute\PropertyAttributeRepository;
use App\Repositories\Backend\Category\VehicleAttribute\VehicleAttributeRepository;
use App\Models\Ad;

class AdRepository implements AdInterface
{
    private $adModel;
    private $jobAttributeRepository;
    private $vehicleAttributeRepository;
    private $propertyAttributeRepository;
    private $electronicAttributeRepository;

    public function __construct(
        Ad $adModel,
        JobAttributeRepository $jobAttributeRepository,
        VehicleAttributeRepository $vehicleAttributeRepository,
        PropertyAttributeRepository $propertyAttributeRepository,
        ElectronicAttributeRepository $electronicAttributeRepository
    ) {
        $this->adModel = $adModel;
        $this->jobAttributeRepository = $jobAttributeRepository;
        $this->vehicleAttributeRepository = $vehicleAttributeRepository;
        $this->propertyAttributeRepository = $propertyAttributeRepository;
        $this->electronicAttributeRepository = $electronicAttributeRepository;
    }

    public function all()
    {
        return $this->adModel->latest()
                    ->search(request('search'))
                    ->filter(request('status'))
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->adModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        $ad = $this->adModel->create([
            'title' => $attributes['title'],
            'slug' => $this->setSlugAttribute($attributes['title']),
            'category_id' => $attributes['category_id'],
            'description' => $attributes['description'],
            'price' => $attributes['price'],
            'status' => $attributes['status'] ?? 0,
            'featured' => $attributes['featured'] ?? 0,
            'city_id' => $attributes['city_id'],
            'address' => $attributes['address'],
            'created_by' => auth()->user()->id,
            'attribute_type' => $attributes['attribute_type'] ?? null,
        ]);

        $attributes['ad_id'] = $ad->id;
        $this->createAttribute($attributes);

        return $ad;
    }

    public function update(array $attributes, $id)
    {
        $ad = $this->adModel->find($id);
        $attributes['ad_id'] = $ad->id;
        $oldType = $ad->attribute_type;

        tap($ad)->update([
            'title' => $attributes['title'],
            'category_id' => $attributes['category_id'],
            'description' => $attributes['description'],
            'price' => $attributes['price'],
            'status' => $attributes['status'] ?? 0,
            'featured' => $attributes['featured'] ?? 0,
            'city_id' => $attributes['city_id'],
            'address' => $attributes['address'],
            'attribute_type' => $attributes['attribute_type'] ?? null,
            'updated_by' => auth()->user()->id
        ]);

        $this->updateAttribute($attributes, $ad, $oldType);

        return $ad;
    }

    public function delete($id)
    {
        return $this->adModel->find($id)->delete();
    }

    private function setSlugAttribute($slug)
    {
        $slug = str_slug($slug);
        $slugs = $this->adModel->whereRaw("slug RLIKE '^{$slug}(-[0-9]*)?$'")
            ->orderBy('id')
            ->pluck('slug');
        if (count($slugs) === 0) {
            return $slug;
        } elseif (!$slugs->isEmpty()) {
            $pieces = explode('-', $slugs);
            $number = (int)end($pieces);
            return $slug .= '-' . ($number + 1);
        }
    }

    private function getAttribute($attribute)
    {
        switch ($attribute) {
            case 'JobAttribute':
                return $this->jobAttributeRepository;
            case 'VehicleAttribute':
                return $this->vehicleAttributeRepository;
            case 'PropertyAttribute':
                return $this->propertyAttributeRepository;
            case 'ElectronicAttribute':
                return $this->electronicAttributeRepository;
            default:
                return null;
        }
    }

    private function createAttribute(array $attributes)
    {
        $attribute = $this->getAttribute($attributes['attribute_type'] ?? null);

        if ($attribute) {
            return $attribute->create($attributes);
        } else {
            return false;
        }
    }

    private function updateAttribute(array $attributes, $ad, $old)
    {
        $attribute = $this->getAttribute($attributes['attribute_type'] ?? null);

        if ($attribute) {
            if ($ad->attribute_type == $old) {
                return $attribute->update($attributes, $ad->attributes->id);
            } else {
                $ad->attributes()->delete();
                return $attribute->create($attributes);
            }
        } else {
            return false;
        }
    }

    public function deletedAll()
    {
        return $this->adModel->onlyTrashed()
                    ->search(request('search'))
                    ->filter(request('status'))
                    ->paginate(10);
    }

    public function restore($id)
    {
        return $this->adModel->withTrashed()->find($id)->restore();
    }

    public function hardDelete($id)
    {
        return $this->adModel->withTrashed()->find($id)->forceDelete();
    }
}
