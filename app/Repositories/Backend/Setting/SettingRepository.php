<?php

namespace App\Repositories\Backend\Setting;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Model;

class SettingRepository implements SettingInterface
{
    private $settingModel;

    public function __construct(Setting $settingModel)
    {
        $this->settingModel = $settingModel;
    }

    public function all()
    {
        return $this->settingModel
                    ->latest()
                    ->paginate(10);
    }

    public function find($id)
    {
        return $this->settingModel->findOrFail($id);
    }

    public function create(array $attributes)
    {
        $dataAttributs =
            [
                'key' => $attributes['key'],
                'value' => $attributes['value'],
            ];

        return $this->settingModel->create($dataAttributs);
    }

    public function update(array $attributes, $id)
    {
        $dataAttributs =
            [
                'key' => $attributes['key'],
                'value' => $attributes['value']
            ];

        $setting = $this->find($id);
        return tap($setting)->update($dataAttributs);
    }

    public function delete($id)
    {
        return $this->settingModel->find($id)->delete();
    }
}
