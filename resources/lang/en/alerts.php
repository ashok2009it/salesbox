<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'city' => [
            'created' => 'City successfully created.',
            'deleted' => 'City successfully deleted.',
            'updated' => 'City successfully updated.',
        ],

        'category' => [
            'created' => 'Category successfully created.',
            'deleted' => 'Category successfully deleted.',
            'updated' => 'Category successfully updated.',
            'error' => 'Error on action',
        ],

        'ad' => [
            'created' => 'Ad successfully created.',
            'deleted' => 'Ad successfully deleted.',
            'updated' => 'Ad successfully updated.',
            'restored' => 'Ad successfully restored.',
            'error' => 'Error on action',
        ],

        'report' => [
            'created' => 'Report successfully created.',
            'deleted' => 'Report successfully deleted.',
            'updated' => 'Report successfully updated.',
            'error' => 'Error on action',
        ],

        'setting' => [
            'created' => 'Setting successfully created.',
            'deleted' => 'Setting successfully deleted.',
            'updated' => 'Setting successfully updated.',
            'error' => 'Error on action',
        ],

        'help' => [
            'help-category' => [
                'created' => 'Help Category successfully created.',
                'deleted' => 'Help Category successfully deleted.',
                'updated' => 'Help Category successfully updated.',
            ],
            'help' => [
                'created' => 'Help successfully created.',
                'deleted' => 'Help successfully deleted.',
                'updated' => 'Help successfully updated.',
            ],
            'error' => 'Error on action',
        ]
    ],

    'frontend' => [
        //
    ],
];
