<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'city' => [
                'title'      => 'City',
                'create'     => 'Add City',
                'edit'       => 'Edit City',

                'table' => [
                    'name' => 'Name',
                    'actions'     => 'Actions',
                ],

                'form' => [
                    'name' => 'Name'
                ],
            ],

            'ad' => [
                'title'      => 'Ad',
                'create'     => 'Add Ad',
                'edit'       => 'Edit Ad',
                'show'       => 'Ad Detail',
                'table' => [
                    'title' => 'Title',
                    'status' => 'Status',
                    'created_by' => 'Created By',
                    'updated_by' => 'Updated By',
                    'actions' => 'Actions',
                ],

                'form' => [
                    'title' => 'Title',
                    'city' => 'City',
                    'address' => 'Address',
                    'price' => 'Price',
                    'description' => 'Description',
                    'image' => 'Image',
                    'status' => 'Status',
                    'featured' => 'Featured',
                ],

                'status' => [
                    'all' => 'All',
                    'active' => 'Active',
                    'inactive' => 'InActive'
                ]
            ],

            'category' => [
                'title'      => 'Category',
                'create'     => 'Add Category',
                'edit'       => 'Edit Category',
                'show'       => 'Category Detail',
                'table' => [
                    'title' => 'Title',
                    'status' => 'Status',
                    'actions' => 'Actions',
                ],

                'form' => [
                    'parent_id' => 'Select Parent Category',
                    'attribute_type' => 'Attribute Type',
                    'title' => 'Title',
                    'icon' => 'Fa Icon',
                    'status' => 'Status',
                    'attributes' => 'Attributes',
                ],
            ],

            'report' => [
                'title'      => 'Report',
                'edit'       => 'Edit Report',
                'show'       => 'Report Detail',
                'table' => [
                    'id' => 'Id',
                    'user' => 'User',
                    'status' => 'Status',
                    'actions' => 'Actions',
                ],
                'form' => [
                    'message' => 'Message',
                    'status' => 'Status',
                ],
                'status' => [
                    'all' => 'All',
                    'active' => 'Pending',
                    'inactive' => 'Completed'
                ]
            ],

            'setting' => [
                'title'      => 'Setting',
                'create'     => 'Add Setting',
                'edit'       => 'Edit Setting',
                'show'       => 'Setting Detail',
                'table' => [
                    'id' => 'Id',
                    'key' => 'Key',
                    'value' => 'Value',
                    'actions' => 'Actions',
                ],
                'form' => [
                    'key' => 'Key',
                    'value' => 'Value',
                ],
            ],

            'help' => [
                'help-category' => [
                    'title'      => 'Help Category',
                    'create'     => 'Add Help Category',
                    'edit'       => 'Edit Help Category',
                    'table' => [
                        'id' => 'Id',
                        'title' => 'Title',
                        'parent' => 'Parent',
                        'actions' => 'Actions',
                    ],
                    'form' => [
                        'title' => 'Title',
                        'parent' => 'Parent',
                    ],
                ],
                'help' => [
                    'title'      => 'Help',
                    'create'     => 'Add Help',
                    'edit'       => 'Edit Help',
                    'show'       => 'Help Detail',
                    'table' => [
                        'question' => 'Question',
                        'popular' => 'Popular',
                        'status' => 'Status',
                        'actions' => 'Actions',
                    ],
                    'form' => [
                        'question' => 'Question',
                        'answer' => 'Answer',
                        'popular' => 'Popular',
                        'status' => 'Status',
                        'category' => 'Category',
                    ],
                ],
            ],

            'user' => [
                'title' => 'User'
            ],
            
            'role' => [
                'title' => 'Role'
            ]
        ],
    ],

    'frontend' => [
        //
    ],
];
