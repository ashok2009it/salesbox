@extends('layouts.frontend')

@section('contents')
<div id="front-app">
    <message :users="{{ $users }}" :id="{{ Auth::user()->id }}"/> 
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ mix('/js/frontend-app.js') }}"></script>
@endsection
