<div class="form-group{{ $errors->has('role.*') ? ' has-error' : '' }} row">
    {!! Form::label('role', 'Role', ['class' => 'col control-label']) !!}

    <div class="col-8">
    	@foreach($roles as $role)
    		<span @if($role->name=='admin') style="display: none;" @endif>
	        	{!! Form::checkbox('role[]', $role->name, $user->hasRole($role->name)) !!} {{ $role->name }}  
	        	<br>
	        </span>
		@endforeach
    </div>
</div>