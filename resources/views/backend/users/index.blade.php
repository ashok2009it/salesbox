@extends('layouts.backend')

@section('header')
    @can('add_users')
        <a href="{{ route('dashboard.users.create') }}">
            <button class="btn btn-success fa fa-plus">&nbsp;Add User</button>
        </a>
    @endcan

    <div class="dropdown d-inline">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
            @if(request('status') != null)
                {{ request('status') }}
            @else
                Filter by Status
            @endif
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <li class="dropdown-item">
                <a href="{{ route('dashboard.users.index') }}">
                    All
                </a>
            </li>
            <li class="dropdown-item">
                <a href="{{ route('dashboard.users.index', ['filter_by' => 'status', 'status' => 'Active']) }}">
                    Active
                </a>
            </li>
            <li class="dropdown-item">
                <a href="{{ route('dashboard.users.index', ['filter_by' => 'status', 'status' => 'InActive']) }}">
                    InActive
                </a>
            </li>
        </ul>
    </div>
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-title">{{ str_plural( __('labels.backend.access.user.title')) }}</h3>
                    </div>

                    <div class="card-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Created</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->created_at->diffForHumans() }}</td>
                            <td>
                                @if($user->status == 1)
                                    Active
                                @else
                                    Pending
                                @endif
                            </td>
                            <td>
                                @can('edit_users')
                                    <a href="{{ route('dashboard.users.edit', $user->id) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit">
                                        <button class="btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i></button>
                                    </a>
                                @endcan

                                @can('delete_users')
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['dashboard.users.destroy', $user->id]
                                    ]) !!}
                                    <button class="btn btn-danger btn-sm" style="display: inline-block; float: left; margin-right: 10px;" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                {!! Form::close() !!}
                                @endcan
                                @hasrole('admin')
                                    @if((Auth::user()->id != $user->id) && !Session::has('original_user'))
                                        <a href="#" class="btn btn-warning btn-sm" title="Login As {{ $user->name }}" onclick="document.getElementById('user-{{ $user->id }}').submit();">
                                            <i class="fa fa-sign-in" aria-hidden="true"></i>
                                        </a>
                                        {!! Form::open(['route' => ['login-as.update', $user->id], 'method' => 'put', 'class' => 'd-none', 'id' => 'user-'.$user->id ]) !!}
                                            <button class="btn btn-link">
                                                <i class="fa fa-sign-in" aria-hidden="true"></i>
                                            </button>
                                        {!! Form::close() !!}
                                    @endif
                                @endhasrole
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    {{ $users->appends(request()->input())->links() }}
                </div>
                    <!-- /.card-body -->
            </div>
                <!-- /.card -->
        </div>
    </div>
@endsection
