<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
    {!! Form::label('name', 'Name', ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' ]) !!}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} clearfix row">
    {!! Form::label('email', 'Email', ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' ]) !!}

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    {!! Form::label('password', 'Password', ['class' => 'col control-label']) !!}

    <div class="col-8">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

        @if ($errors->has('password'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="password-confirm" class="col-md-4 col-form-label">Confirm Password</label>

    <div class="col-md-8">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} clearfix row">
    {!! Form::label('Image', 'Image', ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::file('image', ['class' => 'custom-file-input', 'id' => 'inputGroupFile01', 'multiple']) !!}
        <label class="custom-file-label" id="custom-label" for="inputGroupFile01">Choose file</label>
        
        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>
</div>