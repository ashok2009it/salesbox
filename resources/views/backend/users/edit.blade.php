@extends('layouts.backend')

@section('header')
	{{ $user->name }}
@endsection

@section('contents')
	<div class="container-fluid">
		<div class="row">
			<div class="col-3">
				<div class="card">
				  <img class="card-img-top" src="/images/user/user-thumb.jpg" alt="Card image cap">
				  <div class="card-body" style="text-align: center">
				    <h5 class="card-title">{{ $user->name }}</h5>
				  </div>
				</div>
			</div>
			<div class="col-9">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
				    	<a class="nav-link active" href="#user" role="tab" data-toggle="tab">Edit User</a>
				  	</li>
					<li class="nav-item">
						<a class="nav-link" href="#profile" role="tab" data-toggle="tab">Profile</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#role" role="tab" data-toggle="tab">Role</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  	<div role="tabpanel" class="tab-pane in active" id="user">
				  		<div class="card">
  							<div class="card-body">
						  		{!! Form::model($user, ['method' => 'patch', 'route' => ['dashboard.users.update', $user->id], 'files' => 'true']) !!}
			                    	@include('backend.users._form')

			                    	{!! Form::submit('Edit', ['class' => 'btn btn-success save']) !!}
			                	{!! Form::close() !!}
			                </div>
			            </div>
				  	</div>
				  	<div role="tabpanel" class="tab-pane fade" id="profile">
				  		<div class="card">
  							<div class="card-body">
	  							Coming soon
	  						</div>
  						</div>
  					</div>
					@hasrole('admin')
					  <div role="tabpanel" class="tab-pane fade" id="role">
					  	<div class="card">
  							<div class="card-body">
								{!! Form::model($user, ['method' => 'post', 'route' => ['dashboard.assign.role', $user->id]]) !!}
								  	@include('backend.users.role')
								  	{!! Form::submit('Assignrole', ['class' => 'btn btn-success save']) !!}
		    					{!! Form::close() !!}
		    				</div>
		    			</div>
					  </div>
				  	@endhasrole
				</div>
			</div>
		</div>
	</div>
@endsection