<div class="form-group{{ $errors->has('name') ? ' invalid' : '' }} row">
    {!! Form::label('name', __('labels.backend.access.city.form.name'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}

        @if ($errors->has('name'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
