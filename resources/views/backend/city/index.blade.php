@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.cities.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;{{ __('buttons.general.add') }}</button>
    </a>
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ str_plural( __('labels.backend.access.city.title')) }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('labels.backend.access.city.table.name') }}</th>
                                <th>{{ __('labels.backend.access.city.table.actions') }}</th>
                            </tr>
                            @foreach($cities as $city)
                            <tr>
                                <td>{{ $city->name }}</td>
                                <td>
                                    @can('edit_cities')
                                        <a href="{{ route('dashboard.cities.edit', ['id' => $city->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                    @endcan
                                    @can('delete_cities')
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.cities.destroy', $city->id]
                                        ]) !!}
                                        <button class="btn btn-danger btn-sm" style="display: inline-block; float: left;" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $cities->appends(request()->input())->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
