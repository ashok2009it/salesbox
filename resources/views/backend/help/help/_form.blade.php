<div class="form-group{{ $errors->has('question') ? ' invalid' : '' }} row">
    {!! Form::label('question', __('labels.backend.access.help.help.form.question'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('question', null, ['class' => 'form-control', 'required' ]) !!}

        @if ($errors->has('question'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('question') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('category_id') ? ' invalid' : '' }} row">
    {!! Form::label('category_id', __('labels.backend.access.help.help.form.category'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required', 'placeholder' => 'Select category']) !!}

        @if ($errors->has('category_id'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('answer') ? ' invalid' : '' }} row">
    {!! Form::label('answer', __('labels.backend.access.help.help.form.answer'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::textarea('answer', null, ['class' => 'form-control custom-textarea']) !!}

        @if ($errors->has('answer'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('answer') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('status', __('labels.backend.access.help.help.form.status'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::checkbox('status', 1) !!}
        
        @if ($errors->has('status'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('popular') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('popular', __('labels.backend.access.help.help.form.popular'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::checkbox('popular', 1) !!}
        
        @if ($errors->has('popular'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('popular') }}</strong>
            </span>
        @endif
    </div>
</div>

