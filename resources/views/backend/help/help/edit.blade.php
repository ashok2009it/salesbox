@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.help.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ __('labels.backend.access.Ad.edit') }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                    {!! Form::model($help, ['method' => 'put', 'route' => ['dashboard.help.update', $help->id]]) !!}

                        @include('backend.help.help._form')

                        {!! Form::submit(__('buttons.general.crud.edit'), ['class' => 'btn btn-success save']) !!}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
