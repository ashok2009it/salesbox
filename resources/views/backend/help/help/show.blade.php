@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.help.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ __('labels.backend.access.help.help.show') }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Status: </strong> {{ $help::STATUS[$help->status] ?? Error }}
                            </li>
                            <li class="list-group-item">
                                <strong>Popular: </strong> {{ $help->popular ? 'Popular' : 'Not' }}
                            </li>
                            <li class="list-group-item">
                                <strong>Question: </strong> {{ $help->question }}
                            </li>
                            <li class="list-group-item">
                                <strong>Answer: </strong> <br>
                                {!! $help->answer !!}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

