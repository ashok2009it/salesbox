@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.help.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;{{ __('buttons.general.add') }} </button>
    </a>
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ str_plural(__('labels.backend.access.help.help.title')) }}&nbsp;</h3>
                    <div class="card-tools">

                        <form>
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="search" class="form-control float-right" placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                @foreach(__('labels.backend.access.help.help.table') as $name)
                                    <th>{{ $name }}</th>
                                @endforeach
                            </tr>
                            @foreach($helps as $help)
                            <tr>
                                <td>{{ $help->question }}</td>
                                <td>
                                    @if($help->popular)
                                        Yes
                                    @else
                                        No
                                    @endif
                                </td>
                                <td>{{ $help::STATUS[$help->status] }}</td>
                                <td>
                                    <a
                                        href="{{ route('dashboard.help.show', ['id' => $help->id]) }}"
                                        style="display: inline-block; float: left; margin-right: 10px;"
                                        title="View Detail"
                                        >
                                        <button class="btn btn-success btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </a>
                                    @can('edit_helps')
                                        <a 
                                            href="{{ route('dashboard.help.edit', ['id' => $help->id]) }}"
                                            style="display: inline-block; float: left; margin-right: 10px;"
                                            title="Edit"
                                            >
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                    @endcan
                                    @can('delete_helps')
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.help.destroy', $help->id]
                                        ]) !!}
                                        <button
                                            class="btn btn-danger btn-sm"
                                            style="display: inline-block; float: left;"
                                            onclick="return confirm('Are you sure?')"
                                            >
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $helps->appends(request()->input())->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
