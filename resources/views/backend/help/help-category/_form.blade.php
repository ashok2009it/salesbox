<div class="form-group required {{ $errors->has('parent_id') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('title', __('labels.backend.access.help.help-category.form.parent'), ['class' => 'col control-label']) !!}
    
    <div class="col-8">
        {!! Form::select('parent_id', $categories, null, ['class' => 'form-control', 'placeholder' => 'select Category parent']) !!}

        @if ($errors->has('parent_id'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('parent_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('title') ? ' invalid' : '' }} row">
    {!! Form::label('title', __('labels.backend.access.help.help-category.form.title'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}

        @if ($errors->has('title'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
