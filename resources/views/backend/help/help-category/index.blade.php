@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.help-categories.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;{{ __('buttons.general.add') }}</button>
    </a>
@endsection

@section('contents')
<div class="container-fluid">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ __('labels.backend.access.category.title') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            @foreach(__('labels.backend.access.help.help-category.table') as $name)
                                <th>{{ $name }}</th>
                            @endforeach
                        </tr>
                        @foreach($categories as $category)
                        <tr>
                            <td>
                                {{ $category->id }}
                            </td>
                            <td>
                                <a href="{{ route('dashboard.help-categories.index', ['parent' => $category->id]) }}" title="View Sub Category">
                                    {{ $category->title }}
                                </a>
                            </td>
                            <td>
                                @if ($category->parent_id)
                                    {{ $category->parent->title }} - {{ $category->parent_id }}
                                @else
                                    NA
                                @endif
                            </td>
                            <td>
                                @can('edit_categories')
                                    <a
                                        href="{{ route('dashboard.help-categories.edit', ['id' => $category->id]) }}"
                                        style="display: inline-block; float: left; margin-right: 10px;"
                                        title="Edit"
                                        >
                                        <button class="btn btn-success btn-sm">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </button>
                                    </a>
                                @endcan
                                
                                @can('delete_categories')
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.help-categories.destroy', $category->id]
                                    ]) !!}
                                    @if ($category->help()->count() == 0 && $category->children()->count() == 0)
                                        <button
                                            class="btn btn-danger btn-sm"
                                            style="display: inline-block; float: left;"
                                            onclick="return confirm('Are you sure?')"
                                            >
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    @else
                                        <button
                                            class="btn btn-danger btn-sm"
                                            style="display: inline-block; float: left;"
                                            disabled
                                            title="category has children"
                                            >
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    @endif
                                    {!! Form::close() !!}
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $categories->appends(request()->input())->links() }}
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection
