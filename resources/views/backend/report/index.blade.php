@extends('layouts.backend')

@section('header')
{{--     <a href="{{ route('dashboard.report.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;{{ __('buttons.general.add') }}</button>
    </a> --}}
    <div class="dropdown d-inline">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
            @if(request('status') != null)
                {{ request('status') }}
            @else
                Filter by Status
            @endif
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            
            @foreach(__('labels.backend.access.report.status') as $status)
                @if ($status == 'All')
                    <li class="dropdown-item">
                        <a href="{{ route('dashboard.reports.index') }}">
                            {{ $status }}
                        </a>
                    </li>
                @else
                    <li class="dropdown-item">
                        <a href="{{ route('dashboard.reports.index', ['filter_by' => 'status', 'status' => $status]) }}">
                            {{ $status }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ str_plural(__('labels.backend.access.report.title')) }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                @foreach(__('labels.backend.access.report.table') as $name)
                                    <th>{{ $name }}</th>
                                @endforeach
                            </tr>
                            @foreach($reports as $report)
                                <tr>
                                    <td>{{ $report->id }}</td>
                                    <td>{{ $report->user->name }}</td>
                                    <td>{{ $report::STATUS[$report->status] }}</td>
                                    <td>
                                        <a href="{{ route('dashboard.reports.show', ['id' => $report->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="View Detail">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </a>
                                        @can('edit_reports')
                                            <a href="{{ route('dashboard.reports.edit', ['id' => $report->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit">
                                                <button class="btn btn-success btn-sm">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </button>
                                            </a>
                                        @endcan
                                        @can('delete_reports')
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.reports.destroy', $report->id]
                                            ]) !!}
                                            <button class="btn btn-danger btn-sm" style="display: inline-block; float: left;" onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $reports->appends(request()->input())->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
