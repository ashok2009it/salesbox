@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.reports.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ __('labels.backend.access.report.show') }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                        <strong>Message: </strong>{{$report->message}}<br>
                        <strong>Status: </strong>{{$report::STATUS[$report->status]}}<br>
                        <strong>User: </strong>{{$report->user->name}}<br>
                        <strong>Ad: </strong>{{$report->ad->title}}<br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

