<div class="form-group{{ $errors->has('message') ? ' invalid' : '' }} row">
    {!! Form::label('message', __('labels.backend.access.report.form.message'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::textarea('message', null, ['class' => 'form-control', 'required']) !!}

        @if ($errors->has('message'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' invalid' : '' }} row">
    {!! Form::label('status', __('labels.backend.access.report.form.status'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::checkbox('status', 1) !!}

        @if ($errors->has('status'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
