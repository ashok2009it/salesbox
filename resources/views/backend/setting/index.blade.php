@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.settings.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;{{ __('buttons.general.add') }}</button>
    </a>
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ str_plural(__('labels.backend.access.setting.title')) }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                @foreach(__('labels.backend.access.setting.table') as $name)
                                    <th>{{ $name }}</th>
                                @endforeach
                            </tr>
                            @foreach($settings as $setting)
                                <tr>
                                    <td>{{ $setting->id }}</td>
                                    <td>{{ $setting->key }}</td>
                                    <td>{{ $setting->value }}</td>
                                    <td>
                                        @can('edit_settings')
                                            <a href="{{ route('dashboard.settings.edit', ['id' => $setting->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit">
                                                <button class="btn btn-success btn-sm">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </button>
                                            </a>
                                        @endcan
                                        @can('delete_settings')
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.settings.destroy', $setting->id]
                                            ]) !!}
                                            <button class="btn btn-danger btn-sm" style="display: inline-block; float: left;" onclick="return confirm('Are you sure?')">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $settings->appends(request()->input())->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
