<div class="form-group{{ $errors->has('key') ? ' invalid' : '' }} row">
    {!! Form::label('key', __('labels.backend.access.setting.form.key'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('key', null, ['class' => 'form-control', 'required']) !!}

        @if ($errors->has('key'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('key') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('value') ? ' invalid' : '' }} row">
    {!! Form::label('value', __('labels.backend.access.setting.form.value'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('value', null, ['class' => 'form-control', 'required']) !!}

        @if ($errors->has('value'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('value') }}</strong>
            </span>
        @endif
    </div>
</div>
