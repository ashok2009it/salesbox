@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.settings.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ __('labels.backend.access.setting.create') }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                    {!! Form::model(null, ['method' => 'post', 'route' => ['dashboard.settings.store']]) !!}
                        
                        @include('backend.setting._form')

                        {!! Form::submit(__('buttons.general.crud.create') , ['class' => 'btn btn-success save']) !!}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

