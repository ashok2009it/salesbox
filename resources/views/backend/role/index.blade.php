@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.roles.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;Add Role</button>
    </a>
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ str_plural( __('labels.backend.access.role.title')) }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Display_name</th>
                                <th>Action</th>
                            </tr>
                            @foreach($roles as $role)
                            <tr>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->display_name }}</td>
                                <td>
                                    @hasrole('admin')
                                    <a href="{{ route('dashboard.roles.edit', ['id' => $role->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit">
                                        <button class="btn btn-success btn-sm">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </button>
                                    </a>
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.roles.destroy', $role->id]
                                    ]) !!}
                                    <button class="btn btn-danger btn-sm" style="display: inline-block; float: left;" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    {!! Form::close() !!}
                                    @endhasrole
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
