<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
    {!! Form::label('name', 'Name', ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' ]) !!}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }} row">
    {!! Form::label('display_name', 'Display Name', ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('display_name', null, ['class' => 'form-control' ]) !!}

        @if ($errors->has('display_name'))
            <span class="help-block">
                <strong>{{ $errors->first('display_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }} clearfix row">
    {!! Form::label('remarks', 'remarks', ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::textarea('remarks', null, ['class' => 'form-control custom-textarea']) !!}

        @if ($errors->has('remarks'))
            <span class="help-block">
                <strong>{{ $errors->first('remarks') }}</strong>
            </span>
        @endif
    </div>
</div>

@if(\Route::current()->getName() == 'dashboard.role.edit')
    <div class="form-group clearfix row">
        {!! Form::label('Permissions', 'Permissions', ['class' => 'col-md-12']) !!}
        <hr>
        @foreach($permissions as $permission) 
            <div class="col-md-3">
                {!! Form::checkbox('permission[]', $permission->name, $role->hasPermissionTo($permission->name), [$role->name=='admin' ? 'disabled' : '']) !!} {{ implode(" ",explode("_",$permission->name)) }}
            </div>
        @endforeach
    </div>
@endif
