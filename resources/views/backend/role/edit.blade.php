@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.roles.index') }}" class="btn btn-warning float-right">Back to Listing</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Edit Role</h3>
                </div>
                <div class="card-body">
	                <div class="card-text">
	                {!! Form::model($role, ['method' => 'put', 'route' => ['dashboard.roles.update', $role->id], 'files' => 'true']) !!}
	                    @include('backend.role._form')

	                    {!! Form::submit('Edit', ['class' => 'btn btn-success save']) !!}
	                {!! Form::close() !!}
	            	</div>
            	</div>
        	</div>
        </div>
	</div>
@endsection

