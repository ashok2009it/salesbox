<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ route('frontend.home') }}" class="brand-link">
    {{-- <img src="" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8"> --}}
    <span class="brand-text font-weight-light">{{ config('app.name') }}</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="/images/user/user-thumb.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
{{--         <li class="nav-item">
          <a href="{{ route('dashboard.dashboard') }}" class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}">
            <i class="nav-icon fa fa-dashboard"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li> --}}
        @can('view_cities')
            <li class="nav-item">
                <a href="{{ route('dashboard.cities.index') }}" class="nav-link 
                {{ Request::is('dashboard/cities') ||
                Request::is('dashboard/cities/*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-building"></i>
                    <p>
                        {{ str_plural( __('labels.backend.access.city.title')) }}
                    </p>
                </a>
            </li>
        @endcan
        <li class="nav-item">
            <a href="{{ route('dashboard.categories.index') }}" class="nav-link 
            {{ Request::is('dashboard/categories') ||
            Request::is('dashboard/categories/*') ? 'active' : '' }}">
                <i class="nav-icon fa fa-gift"></i>
                <p>
                    {{ str_plural( __('labels.backend.access.category.title')) }}
                </p>
            </a>
        </li>
        @can('view_ads')
            <li class="nav-item">
                <a href="{{ route('dashboard.ads.index') }}" class="nav-link 
                {{ Request::is('dashboard/ads') ||
                Request::is('dashboard/ads/*') ? 'active' : '' }}">
                    <i class="nav-icon fa fa-rss"></i>
                    <p>
                        {{ str_plural( __('labels.backend.access.ad.title')) }}
                    </p>
                </a>
            </li>
        @endcan
        <li class="nav-header">
          Help
        </li>
        @can('view_help_categories')
          <li class="nav-item">
            <a href="{{ route('dashboard.help-categories.index') }}" class="nav-link 
            {{ Request::is('dashboard/help-categories') ||
            Request::is('dashboard/help-categories/*') ? 'active' : '' }}">
              <i class="nav-icon fa fa-list"></i>
              <p>
                {{ str_plural( __('labels.backend.access.help.help-category.title')) }}
              </p>
            </a>
          </li>
        @endcan
        @can('view_helps')
          <li class="nav-item">
            <a href="{{ route('dashboard.help.index') }}" class="nav-link 
            {{ Request::is('dashboard/help') ||
            Request::is('dashboard/help/*') ? 'active' : '' }}">
              <i class="nav-icon fa fa-question"></i>
              <p>
                {{ str_plural( __('labels.backend.access.help.help.title')) }}
              </p>
            </a>
          </li>
        @endcan
        <li class="nav-header">
          Misc
        </li>
        @can('view_users')
          <li class="nav-item">
            <a href="{{ route('dashboard.users.index') }}" class="nav-link 
            {{ Request::is('dashboard/users') ||
            Request::is('dashboard/users/*') ? 'active' : '' }}">
              <i class="nav-icon fa fa-user"></i>
              <p>
                {{ str_plural( __('labels.backend.access.user.title')) }}
              </p>
            </a>
          </li>
        @endcan
        @can('view_reports')
          <li class="nav-item">
            <a href="{{ route('dashboard.reports.index') }}" class="nav-link 
            {{ Request::is('dashboard/reports') ||
            Request::is('dashboard/reports/*') ? 'active' : '' }}">
              <i class="nav-icon fa fa-file"></i>
              <p>
                {{ str_plural( __('labels.backend.access.report.title')) }}
              </p>
            </a>
          </li>
        @endcan
        @can('view_settings')
          <li class="nav-item">
            <a href="{{ route('dashboard.settings.index') }}" class="nav-link 
            {{ Request::is('dashboard/settings') ||
            Request::is('dashboard/settings/*') ? 'active' : '' }}">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                {{ str_plural( __('labels.backend.access.setting.title')) }}
              </p>
            </a>
          </li>
        @endcan
        @can('view_roles')
          <li class="nav-item">
            <a href="{{ route('dashboard.roles.index') }}" class="nav-link 
            {{ Request::is('dashboard/roles') ||
            Request::is('dashboard/roles/*') ? 'active' : '' }}">
              <i class="nav-icon fa fa-key"></i>
              <p>
                {{ str_plural( __('labels.backend.access.role.title')) }}
              </p>
            </a>
          </li>
        @endcan
{{--         <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-pie-chart"></i>
            <p>
              Charts
              <i class="right fa fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="pages/charts/chartjs.html" class="nav-link">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>ChartJS</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pages/charts/flot.html" class="nav-link">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Flot</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="pages/charts/inline.html" class="nav-link">
                <i class="fa fa-circle-o nav-icon"></i>
                <p>Inline</p>
              </a>
            </li>
          </ul>
        </li> --}}
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
