@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.categories.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ __('labels.backend.access.Ad.show') }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                        <strong>Title: </strong>{{$category->title}}<br>

                        @if($category->attributes)
                            <strong>Attributes: </strong>
                            <ul>
                                @foreach(json_decode($category->attributes) as $cat)
                                    <li>{{ $cat }}</li>
                                @endforeach
                            </ul>
                        @endif
                        @foreach($category->children as $cat)
                            {{$cat->title}}
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

