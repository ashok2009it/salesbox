{!! Form::hidden('category_id', $category->id) !!}
{!! Form::hidden('attribute_type', array_flip($types)[$category->attribute_type] ?? null) !!}

<div class="form-group{{ $errors->has('title') ? ' invalid' : '' }} row">
    {!! Form::label('title', __('labels.backend.access.category.form.title'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}

        @if ($errors->has('title'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('icon') ? ' invalid' : '' }} row">
    {!! Form::label('icon', __('labels.backend.access.category.form.icon'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('icon', null, ['class' => 'form-control', 'required', 'placeholder' => 'Eg. fa-sitemap' ]) !!}

        @if ($errors->has('icon'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('icon') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('status', __('labels.backend.access.category.form.status'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::checkbox('status', 1) !!}
        
        @if ($errors->has('status'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('attributes') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('Attributes', __('labels.backend.access.category.form.attributes'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        <div class="row">
            @if ($columns)
                @foreach($columns as $column)
                    <div class="col-md-3">
                        {!! Form::checkbox('attributes[]', $column, $category->attributes ? in_array($column, json_decode($category->attributes)) : false) !!} {{ $column }}
                    </div>
                @endforeach
            @else
                No attribute to select from
            @endif
        </div>
        @if ($errors->has('attributes'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('attributes') }}</strong>
            </span>
        @endif
    </div>
</div>
