@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.categories.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ __('labels.backend.access.category.edit') }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                    {!! Form::model($category, ['method' => 'put', 'route' => ['dashboard.categories.update', $category->id]]) !!}
                        
                        @include('backend.category.subcategory._form')

                        {!! Form::submit('Save', ['class' => 'btn btn-success save']) !!}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

