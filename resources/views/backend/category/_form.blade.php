<div class="form-group required {{ $errors->has('category_id') ? ' invalid' : '' }} clearfix row">
    <label for="category_id" class="col control-label">{{__('labels.backend.access.category.form.parent_id')}}</label>
    <div class="col-8">
        <select name="category_id" id="category_id" class="form-control">
            <option value="">Top Level</option>
            @if(isset($categoriesList))
                @foreach($categoriesList as $cateKey=>$catValue)
                    <option value="{{ $cateKey }}" {{ isset($blogCategory->parent_id) && $cateKey == $blogCategory->parent_id ? 'selected' : '' }}>
                        {{ $catValue }}
                    </option>
                @endforeach
            @endif
        </select>
    </div>
</div>

@if(\Route::current()->getName() == 'dashboard.categories.create')
    <div class="form-group required {{ $errors->has('attribute_type') ? ' invalid' : '' }} clearfix row">
        <label for="type" class="col control-label">{{__('labels.backend.access.category.form.attribute_type')}}</label>
        <div class="col-8">
            {!! Form::select('attribute_type', $types, isset($category) ? array_search($category->attribute_type, $types) : null, ['class' => 'form-control', 'placeholder' => 'Select Attribute Type']) !!}

            @if ($errors->has('attribute_type'))
                <span class="form-text text-danger">
                    <strong>{{ $errors->first('attribute_type') }}</strong>
                </span>
            @endif
        </div>
    </div>    
@endif

<div class="form-group{{ $errors->has('title') ? ' invalid' : '' }} row">
    {!! Form::label('title', __('labels.backend.access.category.form.title'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}

        @if ($errors->has('title'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('icon') ? ' invalid' : '' }} row">
    {!! Form::label('icon', __('labels.backend.access.category.form.icon'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('icon', null, ['class' => 'form-control', 'required', 'placeholder' => 'Eg. fa-sitemap' ]) !!}

        @if ($errors->has('icon'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('icon') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('status', __('labels.backend.access.category.form.status'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::checkbox('status', 1) !!}
        
        @if ($errors->has('status'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
