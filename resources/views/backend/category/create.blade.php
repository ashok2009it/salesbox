@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.categories.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ str_plural(__('labels.backend.access.category.create')) }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                    {!! Form::model(null, ['method' => 'post', 'route' => ['dashboard.categories.store']]) !!}
                        
                        @include('backend.category._form')

                        {!! Form::submit(__('buttons.general.crud.create'), ['class' => 'btn btn-success save']) !!}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

