@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.categories.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;{{ __('buttons.general.add') }}</button>
    </a>
@endsection

@section('contents')
<div class="container-fluid">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ str_plural(__('labels.backend.access.category.title')) }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            @foreach(__('labels.backend.access.category.table') as $name)
                                <th>{{ $name }}</th>
                            @endforeach
                        </tr>
                        @foreach($categories as $category)
                        <tr>
                            <td>
                                <a href="{{ route('dashboard.categories.index', ['sub_cat' => $category->id]) }}" title="View Sub Category">
                                    {{ $category->title }} ({{ $category->children->count() }})
                                </a>
                            </td>
                            <td>{{ $category->status }}</td>
                            <td>
                                @can('view_categories')
                                    <a href="{{ route('dashboard.categories.show', ['id' => $category->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="View Detail">
                                        <button class="btn btn-success btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </a>
                                @endcan
                                @if($category->parent_id == null)
                                    @can('add_categories')
                                        <a href="{{ route('dashboard.create.sub', ['id' => $category->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Add Sub Category">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </a>
                                    @endcan
                                    @can('edit_categories')
                                        <a href="{{ route('dashboard.categories.edit', ['id' => $category->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                    @endcan
                                @else
                                    @can('edit_categories')
                                        <a href="{{ route('dashboard.edit.sub', ['id' => $category->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit Sub Category">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                    @endcan
                                @endif
                                @can('delete_categories')
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.categories.destroy', $category->id]
                                    ]) !!}
                                    <button class="btn btn-danger btn-sm" style="display: inline-block; float: left;" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    {!! Form::close() !!}
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection
