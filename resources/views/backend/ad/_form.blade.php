<div class="form-group{{ $errors->has('title') ? ' invalid' : '' }} row">
    {!! Form::label('title', __('labels.backend.access.ad.form.title'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' ]) !!}

        @if ($errors->has('title'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div id="app">
    <ad-form 
        :categories="{{ $categoriesList }}"
        :errors="{{ $errors }}"
        :old="{{ json_encode(session()->getOldInput()) }}" 
        :ad="{{ isset($ad) ? $ad : '{}' }}"
        :attributes="{{ isset($ad) ? json_encode($ad->attributes) : json_encode(session()->getOldInput()) }}"
    />
</div>

<div class="form-group{{ $errors->has('city_id') ? ' invalid' : '' }} row">
    {!! Form::label('city_id', __('labels.backend.access.ad.form.city'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::select('city_id', $cities, null, ['class' => 'form-control', 'required', 'placeholder' => 'Select city']) !!}

        @if ($errors->has('city_id'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' invalid' : '' }} row">
    {!! Form::label('address', __('labels.backend.access.ad.form.address'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::text('address', null, ['class' => 'form-control']) !!}

        @if ($errors->has('address'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' invalid' : '' }} row">
    {!! Form::label('price', __('labels.backend.access.ad.form.price'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::number('price', null, ['class' => 'form-control' ]) !!}

        @if ($errors->has('price'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' invalid' : '' }} row">
    {!! Form::label('description', __('labels.backend.access.ad.form.description'), ['class' => 'col control-label']) !!}

    <div class="col-8">
        {!! Form::textarea('description', null, ['class' => 'form-control custom-textarea']) !!}

        @if ($errors->has('description'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} clearfix row">
    {!! Form::label('Image', __('labels.backend.access.ad.form.image'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::file('image', ['class' => 'custom-file-input', 'id' => 'inputGroupFile01', 'multiple']) !!}
        <label class="custom-file-label" id="custom-label" for="inputGroupFile01">Choose file</label>
        
        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('status', __('labels.backend.access.ad.form.status'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::checkbox('status', 1) !!}
        
        @if ($errors->has('status'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('featured') ? ' invalid' : '' }} clearfix row">
    {!! Form::label('featured', __('labels.backend.access.ad.form.featured'), ['class' => 'col control-label ']) !!}

    <div class="col-8">
        {!! Form::checkbox('featured', 1) !!}
        
        @if ($errors->has('featured'))
            <span class="form-text text-danger">
                <strong>{{ $errors->first('featured') }}</strong>
            </span>
        @endif
    </div>
</div>
<script src="{{ mix('js/backend-app.js') }}" defer></script>
