@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.ads.create') }}">
       <button class="btn btn-success fa fa-plus">&nbsp;{{ __('buttons.general.add') }} </button>
    </a>
    <a href="{{ route('dashboard.ads.index', ['deleted' => 'true']) }}">
        <button class="btn btn-danger">&nbsp;{{ __('buttons.general.deleted') }} </button>
    </a>
    <div class="dropdown d-inline">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
            @if(request('status') != null)
                {{ request('status') }}
            @else
                Filter by Status
            @endif
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            
            @foreach(__('labels.backend.access.ad.status') as $status)
                @if ($status == 'All')
                    <li class="dropdown-item">
                        <a href="{{ route('dashboard.ads.index') }}">
                            {{ $status }}
                        </a>
                    </li>
                @else
                    <li class="dropdown-item">
                        <a href="{{ route('dashboard.ads.index', ['filter_by' => 'status', 'status' => $status]) }}">
                            {{ $status }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ str_plural(__('labels.backend.access.ad.title')) }}&nbsp;</h3>
                    <div class="card-tools">

                        <form>
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="search" class="form-control float-right" placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                @foreach(__('labels.backend.access.ad.table') as $name)
                                    <th>{{ $name }}</th>
                                @endforeach
                            </tr>
                            @foreach($ads as $ad)
                            <tr>
                                <td>{{ $ad->title }}</td>
                                <td>{{ $ad::STATUS[$ad->status] }}</td>
                                <td>{{ $ad->createdBy->name }}</td>
                                <td>{{ $ad->updatedBy->name }}</td>
                                <td>
                                    
                                    @if($ad->trashed())
                                       <a href="{{ route('dashboard.ads.restore', ['id' => $ad->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="ReStore">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-recycle"></i>
                                            </button>
                                        </a>

                                        <a href="{{ route('dashboard.ads.hard-delete', ['id' => $ad->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Hard Delete" onclick="return confirm('Are you sure?')">
                                            <button class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </a>
                                    @else
                                        <a href="{{ route('dashboard.ads.show', ['id' => $ad->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="View Detail">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </a>
                                        <a href="{{ route('dashboard.ads.edit', ['id' => $ad->id]) }}" style="display: inline-block; float: left; margin-right: 10px;" title="Edit">
                                            <button class="btn btn-success btn-sm">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </a>
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['dashboard.ads.destroy', $ad->id]
                                        ]) !!}
                                        <button class="btn btn-danger btn-sm" style="display: inline-block; float: left;" onclick="return confirm('Are you sure?')">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $ads->appends(request()->input())->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
