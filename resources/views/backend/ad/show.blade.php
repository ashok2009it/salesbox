@extends('layouts.backend')

@section('header')
    <a href="{{ route('dashboard.ads.index') }}" class="btn btn-warning float-right">{{ __('buttons.general.back') }}</a>
@endsection

@section('contents')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">{{ __('labels.backend.access.ad.show') }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <strong>Title: </strong> {{ $ad->title }}
                            </li>
                            <li class="list-group-item">
                                <strong>Category: </strong> {{ $ad->category->title }}
                            </li>
                            <li class="list-group-item">
                                <strong>Price: </strong> {{ $ad->price }}
                            </li>
                            <li class="list-group-item">
                                <strong>Location: </strong> 
                                @if($ad->address)
                                    {{ $ad->address }},
                                @endif
                                {{ $ad->city->name }}
                            </li>
                            <li class="list-group-item">
                                <strong>Status: </strong> {{ $ad::STATUS[$ad->status] ?? Error }}
                            </li>
                            <li class="list-group-item">
                                <strong>Featured: </strong> 
                                @if($ad->featured)
                                    Featured
                                @else
                                    Not Featured
                                @endif
                            </li>
                            <li class="list-group-item">
                                <strong>Created By: </strong> {{ $ad->createdBy->name }}
                            </li>
                            <li class="list-group-item">
                                <strong>Updated By: </strong> {{ $ad->updatedBy->name }}
                            </li>
                            <li class="list-group-item">
                                <strong>Description: </strong> <br>
                                {!! $ad->description !!}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

