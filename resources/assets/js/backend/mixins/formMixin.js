export const FormMixin = {
  props: ['attributes', 'errors', 'post'],
  methods: {
    attributeHas (type) {
      return this.attributes.indexOf(type) > -1
    },
    errorHas (name) {
      if (this.errors[name]) {
        return this.errors[name][0]
      } else {
        return null
      }
    }
  }
}
