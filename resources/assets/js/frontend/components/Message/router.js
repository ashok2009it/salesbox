import Vue from 'vue'
import Router from 'vue-router'
import Index from './message.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: '/message/',
  routes: [
    {
      path: '/',
      component: Index,
    },
    {
      path: '/:id',
      name: 'user',
      component: Index,
    },
  ],
})

