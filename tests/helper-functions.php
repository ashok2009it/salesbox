<?php

function createUserByRole($role)
{
    $user = factory('App\Models\User')->create();
    factory('Spatie\Permission\Models\Role')
        ->create(['name' => $role]);

    $user->assignRole($role);

    return $user;
}

function createUserWithPermission($permission)
{
    $user = factory('App\Models\User')->create();

    $permissions = is_array($permission)
        ? $permission
        : [$permission];

    foreach($permissions as $permission) {
        factory('Spatie\Permission\Models\Permission')
            ->create(['name' => $permission]);
        $user->givePermissionTo($permission);
    }

    return $user;
}

function createPermission($permission)
{
    $permissions = is_array($permission)
        ? $permission
        : [$permission];

    foreach($permissions as $permission) {
        factory('Spatie\Permission\Models\Permission')
            ->create(['name' => $permission]);
    }
}
