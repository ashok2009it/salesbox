<?php

namespace Tests\Feature\Permission;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function authorized_user_can_see_categories_index_page_or_list_of_categories()
    {
        $user = createUserWithPermission([
            'view_categories',
            'view_dashboards',
        ]);

        $this->actingAs($user)
            ->get('/dashboard/categories')
            ->assertStatus(200);
    }
    /** @test */
    public function unauthorized_user_cannot_see_categories_index_page_or_list_of_categories()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('view_categories');

        $this->actingAs($user)
            ->get('/dashboard/categories')
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_view_create_categories_page()
    {
        $user = createUserWithPermission([
            'add_categories',
            'view_dashboards',
        ]);

        $this->actingAs($user)
            ->get('/dashboard/categories/create')
            ->assertStatus(200);
    }

    /** @test */
    public function unauthorized_user_cannot_view_create_categories_page()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('add_categories');

        $this->actingAs($user)
            ->get('/dashboard/categories/create')
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_view_edit_categories_page()
    {
        $user = createUserWithPermission([
            'edit_categories',
            'view_dashboards',
        ]);
        $category = factory('App\Models\Category')
            ->create();

        $this->actingAs($user)
            ->get('/dashboard/categories/'.$category->id.'/edit')
            ->assertStatus(200);
    }

    /** @test */
    public function unauthorized_user_cannot_view_edit_categories_page()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('edit_categories');
        $category = factory('App\Models\Category')
            ->create();

        $this->actingAs($user)
            ->get('/dashboard/categories/'.$category->id.'/edit')
            ->assertStatus(403);
    }

    /** @test */
    public function unauthorized_user_cannot_update_categories()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('edit_categories');
        $category = factory('App\Models\Category')
            ->create();

        $this->actingAs($user)
            ->put('/dashboard/categories/'.$category->id, $category->toarray())
            ->assertStatus(403);
    }
}
