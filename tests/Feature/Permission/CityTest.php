<?php

namespace Tests\Feature\Permission;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CityTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function authorized_user_can_see_cities_index_page_or_list_of_cities()
    {
        $user = createUserWithPermission([
            'view_cities',
            'view_dashboards',
        ]);

        $this->actingAs($user)
            ->get('/dashboard/cities')
            ->assertStatus(200);
    }
    /** @test */
    public function unauthorized_user_cannot_see_cities_index_page_or_list_of_cities()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('view_cities');

        $this->actingAs($user)
            ->get('/dashboard/cities')
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_view_create_cities_page()
    {
        $user = createUserWithPermission([
            'add_cities',
            'view_dashboards',
        ]);

        $this->actingAs($user)
            ->get('/dashboard/cities/create')
            ->assertStatus(200);
    }

    /** @test */
    public function unauthorized_user_cannot_view_create_cities_page()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('add_cities');

        $this->actingAs($user)
            ->get('/dashboard/cities/create')
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_view_edit_cities_page()
    {
        $user = createUserWithPermission([
            'edit_cities',
            'view_dashboards',
        ]);
        $city = factory('App\Models\City')
            ->create();

        $this->actingAs($user)
            ->get('/dashboard/cities/'.$city->id.'/edit')
            ->assertStatus(200);
    }

    /** @test */
    public function unauthorized_user_cannot_view_edit_cities_page()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('edit_cities');
        $city = factory('App\Models\City')
            ->create();

        $this->actingAs($user)
            ->get('/dashboard/cities/'.$city->id.'/edit')
            ->assertStatus(403);
    }

    /** @test */
    public function unauthorized_user_cannot_update_cities()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('edit_cities');
        $city = factory('App\Models\City')
            ->create();

        $this->actingAs($user)
            ->put('/dashboard/cities/'.$city->id, $city->toarray())
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_delete_cities()
    {
        $user = createUserWithPermission([
            'delete_cities',
            'view_dashboards',
        ]);
        $city = factory('App\Models\City')
            ->create();

        $this->actingAs($user)
            ->delete('/dashboard/cities/'.$city->id)
            ->assertStatus(302);
    }

    /** @test */
    public function unauthorized_user_cannot_delete_cities()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('delete_cities');
        $city = factory('App\Models\City')
            ->create();

        $this->actingAs($user)
            ->delete('/dashboard/cities/'.$city->id)
            ->assertStatus(403);
    }
}
