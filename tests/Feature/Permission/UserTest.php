<?php

namespace Tests\Feature\Permission;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function authorized_user_can_see_user_index_page_or_list_of_users()
    {
        $user = createUserWithPermission([
            'view_users',
            'view_dashboards',
        ]);

        $this->actingAs($user)
            ->get('/dashboard/users')
            ->assertStatus(200);
    }

    /** @test */
    public function unauthorized_user_cannot_see_list_of_users_in_index_page()
    {
        createPermission('view_users');
        $user = createUserWithPermission([
            'view_dashboards',
        ]);

        $this->actingAs($user)
            ->get('/dashboard/users')
            ->assertStatus(403);
    }
}
