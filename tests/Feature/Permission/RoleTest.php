<?php

namespace Tests\Feature\Permission;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function authorized_user_can_see_role_index_page_or_list_of_roles()
    {
        $user = createUserWithPermission([
            'view_roles',
            'view_dashboards',
        ]);

        $this->actingAs($user)
            ->get('/dashboard/roles')
            ->assertStatus(200);
    }
    /** @test */
    public function unauthorized_user_cannot_see_role_index_page_or_list_of_roles()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('view_roles');

        $this->actingAs($user)
            ->get('/dashboard/roles')
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_view_edit_roles_page()
    {
        $user = createUserWithPermission([
            'edit_roles',
            'view_dashboards',
        ]);
        $role = factory('Spatie\Permission\Models\Role')
            ->create();

        $this->actingAs($user)
            ->get('/dashboard/roles/'.$role->id.'/edit')
            ->assertStatus(200);
    }

    /** @test */
    public function unauthorized_user_cannot_view_edit_roles_page()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('edit_roles');
        $role = factory('Spatie\Permission\Models\Role')
            ->create();

        $this->actingAs($user)
            ->get('/dashboard/roles/'.$role->id.'/edit')
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_update_roles()
    {
        $user = createUserWithPermission([
            'edit_roles',
            'view_dashboards',
        ]);
        $role = factory('Spatie\Permission\Models\Role')
            ->create();

        $this->actingAs($user)
            ->put('/dashboard/roles/'.$role->id, $role->toarray())
            ->assertStatus(302);
    }

    /** @test */
    public function unauthorized_user_cannot_update_roles()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('edit_roles');
        $role = factory('Spatie\Permission\Models\Role')
            ->create();

        $this->actingAs($user)
            ->put('/dashboard/roles/'.$role->id, $role->toarray())
            ->assertStatus(403);
    }

    /** @test */
    public function authorized_user_can_delete_roles()
    {
        $user = createUserWithPermission([
            'delete_roles',
            'view_dashboards',
        ]);
        $role = factory('Spatie\Permission\Models\Role')
            ->create();

        $this->actingAs($user)
            ->delete('/dashboard/roles/'.$role->id)
            ->assertStatus(302);
    }

    /** @test */
    public function unauthorized_user_cannot_delete_roles()
    {
        $user = createUserWithPermission([
            'view_dashboards',
        ]);
        createPermission('delete_roles');
        $role = factory('Spatie\Permission\Models\Role')
            ->create();

        $this->actingAs($user)
            ->delete('/dashboard/roles/'.$role->id)
            ->assertStatus(403);
    }
}
