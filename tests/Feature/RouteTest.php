<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RouteTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Route Test.
     *
     * @return void
     */
    /** @test */
    public function dashboard_url_test()
    {
        $appURL = env('APP_URL');

        $user = createUserWithPermission([
            'view_dashboards',
            'view_cities',
            'view_categories',
            'view_ads',
            'view_reports',
            'view_settings',
            'view_roles',
            'view_users'
        ]);

        $urls = [
            '/dashboard',
            '/dashboard/cities',
            '/dashboard/categories',
            '/dashboard/ads',
            '/dashboard/reports',
            '/dashboard/settings',
            '/dashboard/users',
            '/dashboard/roles',
        ];

        echo  PHP_EOL;

        foreach ($urls as $url) {
            $response = $this->actingAs($user)->get($url);
            if ((int)$response->status() == 200) {
                echo $appURL . $url . ' (success ✓ )';
                $this->assertTrue(true);
            } else {
                echo  $appURL . $url . ' (FAILED) did not return a 200.';
                $this->assertTrue(false);
            }

            echo  PHP_EOL;
        }
    }
}
