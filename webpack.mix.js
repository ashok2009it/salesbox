let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
   'resources/assets/frontend/plugins/jquery/dist/jquery.min.js',
   'resources/assets/frontend/plugins/jquery-ui/jquery-ui.min.js',
   'resources/assets/frontend/plugins/tether/js/tether.min.js',
   'resources/assets/frontend/plugins/raty/jquery.raty-fa.js',
   'resources/assets/frontend/plugins/bootstrap/dist/js/popper.min.js',
   'resources/assets/frontend/plugins/bootstrap/dist/js/bootstrap.min.js',
   'resources/assets/frontend/plugins/seiyria-bootstrap-slider/dist/bootstrap-slider.js',
   'resources/assets/frontend/plugins/slick-carousel/slick/slick.min.js',
   'resources/assets/frontend/plugins/jquery-nice-select/js/jquery.nice-select.min.js',
   'resources/assets/frontend/plugins/fancybox/jquery.fancybox.pack.js',
   'resources/assets/frontend/plugins/smoothscroll/SmoothScroll.min.js',
   'resources/assets/frontend/js/scripts.js',
], 'public/js/sellsbox.js');

mix.styles([
	'resources/assets/frontend/plugins/jquery-ui/jquery-ui.min.css',
	'resources/assets/frontend/plugins/bootstrap/dist/css/bootstrap.min.css',
	'resources/assets/frontend/plugins/font-awesome/css/font-awesome.css',
	'resources/assets/frontend/plugins/slick-carousel/slick/slick.css',
	'resources/assets/frontend/plugins/slick-carousel/slick/slick-theme.css',
	'resources/assets/frontend/plugins/fancybox/jquery.fancybox.pack.css',
	'resources/assets/frontend/plugins/jquery-nice-select/css/nice-select.css',
	'resources/assets/frontend/plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css',
   'resources/assets/frontend/css/style.css',
   'resources/assets/frontend/css/customLogin.css',
	'resources/assets/frontend/css/custom.css',
], 'public/css/sellsbox.css');

mix.copyDirectory('resources/assets/frontend/images', 'public/images');
mix.copyDirectory('resources/assets/frontend/plugins/font-awesome/fonts', 'public/fonts');

mix.js('resources/assets/js/backend/app.js', 'public/js/backend-app.js');

mix.js('resources/assets/js/frontend/app.js', 'public/js/frontend-app.js');

mix.scripts([
   'resources/assets/backend/plugins/jquery/jquery.min.js',
   'resources/assets/frontend/plugins/jquery-ui/jquery-ui.min.js',
   'resources/assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js',
   // 'resources/backend/plugins/morris/morris.min.js',
   // 'resources/backend/plugins/sparkline/jquery.sparkline.min.js',
   // 'resources/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
   // 'resources/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
   // 'resources/backend/plugins/knob/jquery.knob.js', 
   'resources/assets/backend/plugins/daterangepicker/daterangepicker.js', //add moment js if using
   'resources/assets/backend/plugins/datepicker/bootstrap-datepicker.js',
   // 'resources/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
   'resources/assets/backend/plugins/slimScroll/jquery.slimscroll.min.js',
   // 'resources/backend/plugins/fastclick/fastclick.js',
   'resources/assets/backend/dist/js/adminlte.js',
], 'public/js/backend.js');

mix.styles([
   'resources/assets/backend/plugins/font-awesome/css/font-awesome.min.css',
   'resources/assets/backend/dist/css/adminlte.min.css',
   'resources/assets/backend/plugins/iCheck/flat/blue.css',
   // 'resources/backend/plugins/morris/morris.css',
   // 'resources/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.css',
   'resources/assets/backend/plugins/datepicker/datepicker3.css',
   'resources/assets/backend/plugins/daterangepicker/daterangepicker-bs3.css',
   // 'resources/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
], 'public/css/backend.css');

//if using charts add
// <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
//if using daterange picker
// <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
